package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.Group;
import hr.fer.oop.suhi2357.model.User;

import java.util.List;

public interface IGroupService {
    public Group findById(Long id);

    public void updateGroup(Group group);

    public void removeGroupById(Long id);

    public List<Group> findByOwner (User owner);

    public List<Group> findByMembersContaining (User members);

    public List<Group> findByName (String name);

    public List<Group> getAllGroups();

    public void addMemberToGroup(User newMember, Group group);

    public void inviteMemberToGroup(User invited, Group group);

    public void exitGroup(User exiter, Group group);
}

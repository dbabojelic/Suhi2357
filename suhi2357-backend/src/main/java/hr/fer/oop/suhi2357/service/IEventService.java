package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.Event;

import hr.fer.oop.suhi2357.model.User;

import java.util.List;

public interface IEventService {
    public Event findById(Long id);

    public void updateEvent(Event event);

    public void removeEventById(Long id);
    
    public void addMemberToEvent(User member, Event event);

    public List<Event> findByOwner (User owner);

    public List<Event> findByMembersContaining (User member);

    public List<Event> findByName (String name);

    public List<Event> findByPlace (String place);

    public List<Event> getAllEvents();

    public List<Event> findByCity(City city);
    
    public List<Event> findByMembersNotContaining(User member);
}

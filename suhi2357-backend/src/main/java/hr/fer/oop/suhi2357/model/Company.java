package hr.fer.oop.suhi2357.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="COMPANIES")
public class Company {
	
	public Company(){
		
	}
	
    public Company(String name, String address, CompanyActivity activity, City city) {
		super();
		this.name = name;
		this.address = address;
		this.activity = activity;
		this.city = city;
	}

	@Id
    @GeneratedValue
    private Long Id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "CITY")
    private City city;

    @Column(nullable = false)
    private String address;

    @ManyToOne
    @JoinColumn(name = "ACTIVITY")
    private CompanyActivity activity;
    
    @OneToMany(mappedBy = "company")
    private List<User> employees;

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CompanyActivity getActivity() {
        return activity;
    }

    public void setActivity(CompanyActivity activity) {
        this.activity = activity;
    }
}

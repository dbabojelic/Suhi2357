package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.Event;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.repository.EventRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EventService implements IEventService {
    @Autowired
    private EventRepository repository;

    @Override
    public Event findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void updateEvent(Event event) {
        repository.saveAndFlush(event);
    }

    @Override
    public void removeEventById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Event> findByOwner(User owner) {
        return repository.findEventsByOwner(owner);
    }

    @Override
    public List<Event> findByMembersContaining(User members) {
        return repository.findEventsByMembersContaining(members);
    }

    @Override
    public List<Event> findByName(String name) {
        return repository.findEventsByName(name);
    }

    @Override
    public List<Event> findByPlace(String place) {
        return repository.findEventsByPlace(place);
    }

    @Override
    public List<Event> getAllEvents() {
        return repository.findAll();
    }

    @Override
    public List<Event> findByCity(City city) {
        return repository.findEventsByCity(city);
    }

	@Override
	public void addMemberToEvent(User member, Event event) {
		event.getMembers().add(member);
		updateEvent(event);
	}

	@Override
	public List<Event> findByMembersNotContaining(User member) {
		List<Event> allEvents = repository.findAll();
		List<Event> membersEvents = repository.findEventsByMembersContaining(member);
		List<Event> eventsWithoutMember = new ArrayList<>(allEvents);
		
		eventsWithoutMember.removeAll(membersEvents);
		
		return eventsWithoutMember;
 	}
}

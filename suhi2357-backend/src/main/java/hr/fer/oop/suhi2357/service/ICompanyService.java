package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.CompanyActivity;
import hr.fer.oop.suhi2357.model.User;

import java.util.List;

public interface ICompanyService {
    public Company findById(Long id);

    public void updateCompany(Company company);

    public void removeCompanyById(Long id);

    public List<Company> findByActivity (CompanyActivity activity);

    public List<Company> findByAddress (String address);

    public List<Company> findByName (String name);

    public List<Company> findByEmployeesContaining (User employee);

    public List<Company> getAllCompanies();

    public List<Company> findByCity(City city);
}

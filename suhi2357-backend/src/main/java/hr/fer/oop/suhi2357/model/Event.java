package hr.fer.oop.suhi2357.model;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="EVENTS")
public class Event {
	
	@Id
	@GeneratedValue
	private long Id;
	
	@ManyToOne
	@JoinColumn(name = "OWNER")
	private User owner;
	
	@JsonIgnoreProperties("events")
	@ManyToMany
	@JoinTable(name = "Event_User",
	joinColumns = { @JoinColumn(name = "event_Id") },
	inverseJoinColumns = { @JoinColumn(name = "user_Id") })
	private List<User> members;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String place;

    @ManyToOne
    @JoinColumn(name = "CITY")
    private City city;
	
	@Column(nullable = false)
	private Date time;
	
	public Event() {
	}
	
	public Event(User owner, String name, String place, Date time, City city) {
		this.owner = owner;
		this.name = name;
		this.place = place;
		this.time = time;
		this.city = city;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public List<User> getMembers() {
		return members;
	}

	public void setMembers(List<User> members) {
		this.members = members;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	
}

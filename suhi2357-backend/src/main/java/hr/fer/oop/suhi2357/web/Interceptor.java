package hr.fer.oop.suhi2357.web;

import hr.fer.oop.suhi2357.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Interceptor extends HandlerInterceptorAdapter {
    @Autowired
    private ActiveUserStorage activeUserStorage;
    @Autowired
    private IUserService userService;

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        String email = httpServletRequest.getParameter("username");
        activeUserStorage.addUser(userService.findByEmail(email));
    }
}

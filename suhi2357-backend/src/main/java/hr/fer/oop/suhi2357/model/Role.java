package hr.fer.oop.suhi2357.model;

public enum Role {
    USER, ADMIN, OWNER;
}

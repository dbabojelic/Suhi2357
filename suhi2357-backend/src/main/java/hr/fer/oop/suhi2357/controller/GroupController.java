package hr.fer.oop.suhi2357.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;



import javax.validation.Valid;

import hr.fer.oop.suhi2357.form.GroupForm;
import hr.fer.oop.suhi2357.mail.EmailService;
import hr.fer.oop.suhi2357.model.Group;
import hr.fer.oop.suhi2357.model.Role;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.service.IGroupService;
import hr.fer.oop.suhi2357.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/group")
public class GroupController {
	private final IGroupService groupService;
	private final IUserService userService;

    @Autowired
    private EmailService emailService;
	
	public GroupController(final IGroupService groupService, final IUserService userService) {
		this.groupService = groupService;
		this.userService = userService;
	}

	@GetMapping("/member/invite/{userId}/{groupId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> invite(@PathVariable long userId, @PathVariable long groupId, Principal principal) {
		User user = userService.findById(userId);
		Group group = groupService.findById(groupId);
		User currentUser = userService.findByEmail(principal.getName());
		
		if (group == null) {
			return ResponseEntity.badRequest().build();
		}

		if (!currentUser.equals(group.getOwner()) && currentUser.getRole() == Role.USER)
			return ResponseEntity.badRequest().build();

		groupService.inviteMemberToGroup(user, group);
		return ResponseEntity.ok(HttpStatus.ACCEPTED);
	}

    @GetMapping("/member/invite/accept/{groupId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> acceptInvitation(@PathVariable long groupId, Principal principal) {
        Group group = groupService.findById(groupId);
        User currentUser = userService.findByEmail(principal.getName());

        if (group == null) {
            return ResponseEntity.badRequest().build();
        }

        if (!group.getInvited().contains(currentUser))
            return ResponseEntity.badRequest().build();

        groupService.addMemberToGroup(currentUser, group);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @GetMapping("/member/exit/{groupId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> exitGroup(@PathVariable long groupId, Principal principal) {
        Group group = groupService.findById(groupId);
        User currentUser = userService.findByEmail(principal.getName());

        if (group == null) {
            return ResponseEntity.badRequest().build();
        }

        groupService.exitGroup(currentUser, group);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

	@GetMapping
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getAllGroups() {
		return ResponseEntity.ok(this.groupService.getAllGroups());
	}

	@GetMapping("/name/{name}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getGroupsWithName(@PathVariable String name) {
    	List<Group> groups = new ArrayList<>();
    	List<String> possibilities = Utils.croatianise(name);
    	
    	for (String posiblity : possibilities) {
    		groups.addAll(groupService.findByName(posiblity));
    	}
    	
    	if (groups == null || groups.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(groups);
    }
	
	@GetMapping("/owner/{ownerId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getGroupsWithOwner(@PathVariable long ownerId) {
    	List<Group> groups = groupService.findByOwner(userService.findById(ownerId));
    	if (groups == null || groups.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(groups);
    }
	
	@GetMapping("/groupId/{groupId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getGroupsWithId(@PathVariable long groupId) {
    	Group group = groupService.findById(groupId);
    	if (group == null) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(group);
    }
	
	@GetMapping("/member/{memberId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getGroupsWithMember(@PathVariable long memberId) {
    	List<Group> group = groupService.findByMembersContaining(userService.findById(memberId));
    	if (group == null) { 
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(group);
    }

    @GetMapping("/invited/{invitedId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getGroupsWithInvited(@PathVariable long invitedId) {
        List<Group> group = userService.findById(invitedId).getInvitations();
        if (group == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(group);
    }
	
	@PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> create(@Valid @RequestBody final GroupForm gf, Principal principal) {
	    User owner = userService.findByEmail(principal.getName());
	    String groupName = gf.getName();
		Group newGroup = new Group(owner, groupName);
		System.out.println(gf.getInvitedIds().size());
		for (Long id: gf.getInvitedIds()) {
		    User invitedUser = userService.findById(id);
		    newGroup.getInvited().add(invitedUser);
		    emailService.sendMessage(invitedUser.getEmail(),
                    "Poziv za pridruživanje grupi",
                    "Korisnik " + owner.getFirstName() + " " + owner.getLastName() + " pozvao vas je " +
                            "da se pridružite njegovoj grupi " + groupName + ". To možete učiniti na našoj stranici." +
                        "\nLijep pozdrav, Udruga poduzetnika");

        }
        groupService.updateGroup(newGroup);
		return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
	
	@GetMapping("/remove/{groupId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> remove(@PathVariable long groupId, Principal principal) {
		User u = userService.findByEmail(principal.getName());
		Group group = groupService.findById(groupId);
		if (group == null) {
			return ResponseEntity.badRequest().build();
		}

		if (!u.equals(group.getOwner()) && u.getRole() == Role.USER)
		    return ResponseEntity.badRequest().build();
		
        groupService.removeGroupById(groupId);

        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
}

package hr.fer.oop.suhi2357.loader;

import hr.fer.oop.suhi2357.model.*;
import hr.fer.oop.suhi2357.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final CityRepository cityRepository;
    private final CompanyRepository companyRepository;

    @Autowired
    CompanyActivityRepository companyActivityRepository;

    @Autowired
    public DatabaseLoader(UserRepository repository, GroupRepository repository2, CityRepository repository3, CompanyRepository rep4) {

        this.userRepository = repository;
        this.groupRepository = repository2;
        this.cityRepository = repository3;
        this.companyRepository = rep4;
    }

    @Override
    public void run(String... strings) throws Exception {
//    	this.cityRepository.save(new City("Zagreb"));
//    	this.cityRepository.save(new City("Split"));
//    	this.cityRepository.save(new City("Rijeka"));
//    	this.cityRepository.save(new City("Osijek"));
//    	this.cityRepository.save(new City("Zadar"));
//    	this.cityRepository.save(new City("Slavonski Brod"));
//    	this.cityRepository.save(new City("Pula"));
//    	this.cityRepository.save(new City("Sesvete"));
//    	this.cityRepository.save(new City("Kaštela"));
//    	this.cityRepository.save(new City("Karlovac"));
//    	this.cityRepository.save(new City("Sisak"));
//    	this.cityRepository.save(new City("Dubrovnik"));
//    	this.cityRepository.save(new City("Varaždin"));
//    	this.cityRepository.save(new City("Šibenik"));
//    	this.cityRepository.save(new City("Velika Gorica"));
//    	this.cityRepository.save(new City("Vinkovci"));
//    	this.cityRepository.save(new City("Vukovar"));
//    	this.cityRepository.save(new City("Bjelovar"));
//    	this.cityRepository.save(new City("Koprivnica"));
//    	this.cityRepository.save(new City("Đakovo"));
//
//    	CompanyActivity trgovina = new CompanyActivity("Trgovina");
//    	CompanyActivity it = new CompanyActivity("IT");
//    	this.companyActivityRepository.save(new CompanyActivity("Poljoprivreda"));
//        this.companyActivityRepository.save(it);
//        this.companyActivityRepository.save(trgovina);
//        this.companyActivityRepository.save(new CompanyActivity("Stočarstvo"));
//        this.companyActivityRepository.save(new CompanyActivity("Pčelarstvo"));
//        this.companyActivityRepository.save(new CompanyActivity("Turizam"));
//
//    	this.companyRepository.save(new Company("Agrokor", "Unska 3", trgovina, cityRepository.findCityByName("Zagreb")));
//    	this.companyRepository.save(new Company("Lidl", "Unska 17", trgovina, cityRepository.findCityByName("Zagreb")));
//    	this.companyRepository.save(new Company("Kaufland", "Unska 23", trgovina, cityRepository.findCityByName("Varaždin")));
//    	this.companyRepository.save(new Company("MMA", "Unska 34", trgovina, cityRepository.findCityByName("Pula")));
//
//    	this.companyRepository.save(new Company("FirmaIT", "Unska 456", it, cityRepository.findCityByName("Pula")));
//    	this.companyRepository.save(new Company("ITCrew", "Unska 343", it, cityRepository.findCityByName("Bjelovar")));
//    	this.companyRepository.save(new Company("TEAM", "Unska 2345", it, cityRepository.findCityByName("Karlovac")));
//
//        this.userRepository.save(new User("Zvonimir", "Sučić", companyRepository.findCompaniesByName("Agrokor").get(0), Role.USER, "zvonimir.sucic@fer.hr", "03453", "Menadžer na odjelu voća.", "password", cityRepository.findCityByName("Zagreb"), "Kotlova 6"));
//        this.userRepository.save(new User("Petra", "Mikulić", companyRepository.findCompaniesByName("MMA").get(0), Role.USER, "petra.mikulic@fer.hr", "03453435", "Radim na blagajni.", "password", cityRepository.findCityByName("Pula"), "Preradoviceva 2"));
//        this.userRepository.save(new User("Luka", "Barišić", companyRepository.findCompaniesByName("ITCrew").get(0), Role.USER, "luka.barisic@fer.hr", "03453435", "Najači sam programer na odjelu, programiram u Javi. Java je moj život.", "password", cityRepository.findCityByName("Zagreb"), "Preradoviceva 2"));
//        this.userRepository.save(new User("Marko", "Pisačić", companyRepository.findCompaniesByName("Lidl").get(0), Role.USER, "marko.pisacic@fer.hr", "03453435", "Radim u skladištu, a povremeno na miješalici.", "password", cityRepository.findCityByName("Zagreb"), "Preradoviceva 2"));
//        this.userRepository.save(new User("Ivan", "Pikelj", companyRepository.findCompaniesByName("ITCrew").get(0), Role.USER, "ivan.pikelj@fer.hr", "03453435", "Inženjer računarstva, trenutno sam voditelj projekata.", "password", cityRepository.findCityByName("Bjelovar"), "Preradoviceva 2"));
//        this.userRepository.save(new User("Dario", "Babojelić", companyRepository.findCompaniesByName("ITCrew").get(0), Role.OWNER, "dario.babojelic@fer.hr", "03453435", "CEO, direktor firme.", "password", cityRepository.findCityByName("Bjelovar"), "Preradoviceva 2"));
//        this.userRepository.save(new User("Vjeko", "Žikuna", companyRepository.findCompaniesByName("ITCrew").get(0), Role.ADMIN, "vjeko.kuzina@fer.hr", "03453435", "Programer, java developer", "password", cityRepository.findCityByName("Bjelovar"), "Preradoviceva 2"));
//        this.userRepository.save(new User("Tomica", "Kravaršćan", companyRepository.findCompaniesByName("TEAM").get(0), Role.ADMIN, "tomislav.kravarscan@fer.hr", "03453435", "Voditelj projekta", "password", cityRepository.findCityByName("Zadar"), "Preradoviceva 2"));
//
//        this.groupRepository.save(new Group(userRepository.findUserByEmail("email@email.com"), "mojagrupa"));
    }
}

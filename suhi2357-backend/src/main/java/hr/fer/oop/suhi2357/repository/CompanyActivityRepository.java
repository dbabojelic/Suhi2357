package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.CompanyActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyActivityRepository extends JpaRepository<CompanyActivity, Long> {
	
	public List<CompanyActivity> findCompanyActivityByActivityName (String activityName);

}

package hr.fer.oop.suhi2357.controller;

import java.util.ArrayList;
import java.util.List;

public class Utils {
	
	public static List<String> croatianise(String str) {
		List<String> possibilities = new ArrayList<>();
		List<String> newPossibilities;

		possibilities.add(str);

		char[] string = str.toCharArray();
		for (int i = 0; i < string.length; i++) {
			newPossibilities = new ArrayList<>();

			if (string[i] == 'c' || string[i] == 'd' || string[i] == 'z'
					|| string[i] == 's' || string[i] == 'C' || string[i] == 'D'
					|| string[i] == 'Z' || string[i] == 'S') {
				for (String possibility : possibilities) {
					char[] poss = possibility.toCharArray();
					newPossibilities.add(possibility);

					if (string[i] == 'c') {
						poss[i] = 'ć';
						newPossibilities.add(new String(poss));
						poss[i] = 'č';
						newPossibilities.add(new String(poss));
					} else if (string[i] == 'd') {
						poss[i] = 'đ';
						newPossibilities.add(new String(poss));
					} else if (string[i] == 'z') {
						poss[i] = 'ž';
						newPossibilities.add(new String(poss));
					} else if (string[i] == 's') {
						poss[i] = 'š';
						newPossibilities.add(new String(poss));
					} else if (string[i] == 'C') {
						poss[i] = 'Ć';
						newPossibilities.add(new String(poss));
						poss[i] = 'Č';
						newPossibilities.add(new String(poss));
					} else if (string[i] == 'D') {
						poss[i] = 'Đ';
						newPossibilities.add(new String(poss));
					} else if (string[i] == 'Z') {
						poss[i] = 'Ž';
						newPossibilities.add(new String(poss));
					} else {
						poss[i] = 'Š';
						newPossibilities.add(new String(poss));
					}
				}
				possibilities = newPossibilities;
			}
		}
		
		return possibilities;
	}
}

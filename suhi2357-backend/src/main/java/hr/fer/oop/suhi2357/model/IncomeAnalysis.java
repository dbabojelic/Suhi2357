package hr.fer.oop.suhi2357.model;

import javax.persistence.*;

@Entity
@Table(name="INCOME_ANALYSIS")
public class IncomeAnalysis {
    @Id
    @GeneratedValue
    private Long Id;

    @OneToOne
    private City city;

    @Column
    private Integer incomeCnt;

    public IncomeAnalysis(City city) {
        this.city = city;
        incomeCnt = 1;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Integer getIncomeCnt() {
        return incomeCnt;
    }

    public void setIncomeCnt(Integer incomeCnt) {
        this.incomeCnt = incomeCnt;
    }
}

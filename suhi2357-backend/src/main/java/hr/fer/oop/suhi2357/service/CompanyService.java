package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.CompanyActivity;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService implements ICompanyService {
    @Autowired
    private CompanyRepository repository;

    @Override
    public Company findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void updateCompany(Company company) {
        repository.saveAndFlush(company);
    }

    @Override
    public void removeCompanyById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Company> findByActivity(CompanyActivity activity) {
        return repository.findCompaniesByActivity(activity);
    }

    @Override
    public List<Company> findByAddress(String address) {
        return repository.findCompaniesByAddress(address);
    }

    @Override
    public List<Company> findByName(String name) {
        return repository.findCompaniesByName(name);
    }

    @Override
    public List<Company> findByEmployeesContaining(User employee) {
        return repository.findCompaniesByEmployeesContaining(employee);
    }

    @Override
    public List<Company> getAllCompanies() {
        return repository.findAll();
    }

    @Override
    public List<Company> findByCity(City city) {
        return repository.findCompaniesByCity(city);
    }
}

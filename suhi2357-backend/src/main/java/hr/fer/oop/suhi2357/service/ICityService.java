package hr.fer.oop.suhi2357.service;

import java.util.List;

import hr.fer.oop.suhi2357.model.City;

public interface ICityService {
	public City findById(Long id);
	
	public City findByName(String name);
	
	public void updateCity(City city);
	
	public List<City> getAllCities();
	
	public void removeCityById(Long id);
}

package hr.fer.oop.suhi2357.controller;


import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import hr.fer.oop.suhi2357.form.EventForm;
import hr.fer.oop.suhi2357.mail.EmailService;
import hr.fer.oop.suhi2357.model.*;
import hr.fer.oop.suhi2357.service.ICityService;
import hr.fer.oop.suhi2357.service.IEventService;
import hr.fer.oop.suhi2357.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/event")
public class EventControler {
	
	private final IEventService eventService;
	private final IUserService userService;
	private final ICityService cityService;

	@Autowired
    private EmailService emailService;

	@Value("${backend.host.url}")
    private String hostUrl;

	@Autowired
	public EventControler(IEventService eventService, IUserService userService, ICityService cityService) {
		super();
		this.eventService = eventService;
		this.userService = userService;
		this.cityService = cityService;
	}

	@GetMapping
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getAllEvents() {
		return ResponseEntity.ok(this.eventService.getAllEvents());
	}

	@GetMapping("/eventId/{eventId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getEventWithId(@PathVariable final Long eventId) {
		Event event = eventService.findById(eventId);
		if (event == null)
			return ResponseEntity.badRequest().build();

		return ResponseEntity.ok(event);
    }
	
	@PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> create(@Valid @RequestBody final EventForm ef, Principal principal) {
        eventService.updateEvent(new Event(userService.findByEmail(principal.getName()), ef.getName(), ef.getPlace(), ef.getTime(), cityService.findById(ef.getCityId())));
        List<Event> events = eventService.getAllEvents();
        long id = events.get(events.size()-1).getId();
		List<User> users = userService.getAllUsers();
		for( User user : users ) {
            emailService.sendMessage(user.getEmail(), "Poziv na poduzetnički događaj",
                    "Poštovani,\n\npozvani ste na poduzetnički događaj. Potvrdite svoj dolazak"
                        + " na stranici događaja: " + hostUrl + "event/" + String.valueOf(id)
                        + "\n\n Veselimo se vašem dolasku!");
        }
		return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
	
	@GetMapping("/remove/{eventId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> remove(@PathVariable long eventId, Principal principal) {
		User u = userService.findByEmail(principal.getName());

		Event event = eventService.findById(eventId);
		if (event == null) {
			return ResponseEntity.badRequest().build();
		}

		if (!u.equals(event.getOwner()) && u.getRole() == Role.USER)
		    return ResponseEntity.badRequest().build();
		
        eventService.removeEventById(eventId);
		return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
	
	@GetMapping("/owner/{ownerId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getEventWithOwner(@PathVariable long ownerId) {
    	List<Event> events = eventService.findByOwner(userService.findById(ownerId));
    	if (events == null) { 
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(events);
    }
	
	@GetMapping("/member/{memberId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getEventsWithMember(@PathVariable long memberId) {
    	List<Event> events = eventService.findByMembersContaining(userService.findById(memberId));
    	if (events == null) { 
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(events);
    }
	
	@GetMapping("/notmember/{memberId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getEventsWithoutMember(@PathVariable long memberId) {
    	List<Event> events = eventService.findByMembersNotContaining(userService.findById(memberId));
    	if (events == null) { 
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(events);
    }
	
	@GetMapping("/name/{name}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getEventsWithName(@PathVariable String name) {
    	List<Event> events = new ArrayList<>();
    	List<String> possibilities = Utils.croatianise(name);
    	
    	for (String posiblity : possibilities) {
    		events.addAll(eventService.findByName(posiblity));
    	}
    	
    	if (events == null || events.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(events);
    }
	
	@GetMapping("/place/{place}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getEventsInPlace(@PathVariable String place) {
    	List<Event> events = new ArrayList<>();
    	List<String> possibilities = Utils.croatianise(place);
    	
    	for (String posiblity : possibilities) {
    		events.addAll(eventService.findByPlace(posiblity));
    	}
    	
    	if (events == null || events.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(events);
    }

    @GetMapping("/city/{cityId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getEventsWithCity(@PathVariable long cityId) {
        City city = cityService.findById(cityId);
        if (city == null) {
            return ResponseEntity.badRequest().build();
        }
        List<Event> events = eventService.findByCity(city);

        return ResponseEntity.ok(events);
    }
    
    @GetMapping("/member/add/{userId}/{eventId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> add(@PathVariable long userId, @PathVariable long eventId) {
		User user = userService.findById(userId);
		Event event = eventService.findById(eventId);
		
		if (event == null) {
			return ResponseEntity.badRequest().build();
		}
		
		if (user == null) {
			return ResponseEntity.badRequest().build();
		}
		
		eventService.addMemberToEvent(user, event);
		return ResponseEntity.ok(HttpStatus.ACCEPTED);
	}
}

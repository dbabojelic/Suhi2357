package hr.fer.oop.suhi2357.model;

import javax.persistence.*;

@Entity
@Table(name="ACTIVITIES")
public class CompanyActivity {
    @Id
    @GeneratedValue
    private Long Id;

    @Column(nullable = false)
    private String activityName;
    
    public CompanyActivity() {
    }
    
    public CompanyActivity(String activityName) {
		super();
		this.activityName = activityName;
	}

	public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}

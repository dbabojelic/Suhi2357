package hr.fer.oop.suhi2357.controller;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.IncomeAnalysis;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.service.ICityService;
import hr.fer.oop.suhi2357.service.IIncomeAnalysisService;
import hr.fer.oop.suhi2357.service.IUserService;
import hr.fer.oop.suhi2357.service.IncomeAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/analysis")
public class IncomeAnalysisController {

    @Autowired
    private ICityService cityService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IIncomeAnalysisService incomeAnalysisService;

    @GetMapping("/unregistered")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    ResponseEntity<?> getAnalysis() {
        return ResponseEntity.ok(incomeAnalysisService.getAnalysis());
    }

    @GetMapping("/registered")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    ResponseEntity<?> getAnalysisRegistered() {
        List<IncomeAnalysis> analysis = new ArrayList<>();
        for (City city: cityService.getAllCities()) {
            IncomeAnalysis anal = new IncomeAnalysis(city);
            List<User> thatCityUsers = userService.findByCity(city);
            anal.setIncomeCnt(anal.getIncomeCnt() - 1 + thatCityUsers.size());
            analysis.add(anal);
        }
        return ResponseEntity.ok(analysis);
    }
}

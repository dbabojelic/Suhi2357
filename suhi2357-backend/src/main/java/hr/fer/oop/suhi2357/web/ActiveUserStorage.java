package hr.fer.oop.suhi2357.web;

import hr.fer.oop.suhi2357.model.User;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component("activeUserStorage")
public class ActiveUserStorage {

    private Set<Long> activeUsers;

    public ActiveUserStorage() {
        activeUsers = new HashSet<>();
    }

    public Set<Long> getActiveUsers() {
        return activeUsers;
    }

    public void setActiveUsers(Set<Long> activeUsers) {
        this.activeUsers = activeUsers;
    }

    public void addUser(User u) {
        activeUsers.add(u.getId());
    }

    public void removeUser(User u) {
        activeUsers.remove(u.getId());
    }
}

package hr.fer.oop.suhi2357.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="NOTIFICATIONS")
public class Notification {
	
	@Id
	@GeneratedValue
	private long Id;
	
	@Column(nullable = false)
	private String message;

	@ManyToOne
	@JoinColumn(name = "SENDER")
	private User sender;
	
	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "Notification_User",
	joinColumns = { @JoinColumn(name = "notification_Id") },
	inverseJoinColumns = { @JoinColumn(name = "user_Id") })
	private List<User> recievers;
	
	@Column(nullable = false)
	private NotificationType type;

	public Notification() {
	}
	
	public Notification(String message, User sender, List<User> recievers, NotificationType type) {
		super();
		this.message = message;
		this.sender = sender;
		this.recievers = recievers;
		this.type = type;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public List<User> getRecievers() {
		return recievers;
	}
	
	public void setRecievers(List<User> recievers) {
		this.recievers = recievers;
	}
	
	public NotificationType getType() {
		return type;
	}
	
	public void setType(NotificationType type) {
		this.type = type;
	}
}
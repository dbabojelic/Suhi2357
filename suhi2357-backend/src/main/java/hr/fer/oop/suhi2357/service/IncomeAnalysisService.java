package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.IncomeAnalysis;
import hr.fer.oop.suhi2357.repository.CityRepository;
import hr.fer.oop.suhi2357.repository.IncomeAnalysisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncomeAnalysisService implements IIncomeAnalysisService {
    @Autowired
    private IncomeAnalysisRepository repository;
    @Autowired
    private CityRepository cityRepository;

    @Override
    public List<IncomeAnalysis> getAnalysis() {
        return repository.findAll();
    }

    @Override
    public void addNewVisit(String cityName) {
        cityName = cityName.toLowerCase();
        cityName = cityName.substring(0, 1).toUpperCase() + cityName.substring(1);
        City city = cityRepository.findCityByName(cityName);

        if (city == null)
            return;

        IncomeAnalysis anal = repository.findByCity(city);
        if (anal == null)
            anal = new IncomeAnalysis(city);
        else
            anal.setIncomeCnt(anal.getIncomeCnt() + 1);

        updateIncomeAnalysis(anal);
    }

    @Override
    public void updateIncomeAnalysis(IncomeAnalysis analysis) {
        repository.saveAndFlush(analysis);
    }
}

package hr.fer.oop.suhi2357.mail;

public interface IEmailService {
    void sendMessage(String to, String subject, String message);
}

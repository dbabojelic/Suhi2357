package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.CompanyActivity;
import hr.fer.oop.suhi2357.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.jws.soap.SOAPBinding;
import java.net.CookieHandler;
import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

	public List<Company> findCompaniesByActivity (CompanyActivity activity);

	public List<Company> findCompaniesByAddress (String address);

	public List<Company> findCompaniesByName (String name);

	public List<Company> findCompaniesByEmployeesContaining (User employee);

	public List<Company> findCompaniesByCity(City city);
	



}

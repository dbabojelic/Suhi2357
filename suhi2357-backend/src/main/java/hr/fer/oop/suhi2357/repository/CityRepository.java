package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.City;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
	public City findCityByName(String name);
}

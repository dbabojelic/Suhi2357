package hr.fer.oop.suhi2357.service;


import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.Event;
import hr.fer.oop.suhi2357.model.Group;
import hr.fer.oop.suhi2357.model.Role;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserService implements IUserService {
    @Autowired
    private UserRepository repository;

    @Override
    public User findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void updateUser(User user) {
        repository.saveAndFlush(user);
    }

    @Override
    public void removeUserById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<User> findByFirstName(String firstName) {
        return repository.findUsersByFirstName(firstName);
    }

    @Override
    public List<User> findByLastName(String lastName) {
        return repository.findUsersByLastName(lastName);
    }

    @Override
    public List<User> findByCompany(Company company) {
        return repository.findUsersByCompany(company);
    }

    @Override
    public List<User> findByRole(Role role) {
        return repository.findUsersByRole(role);
    }

    @Override
    public User findByEmail(String email) {
        return repository.findUserByEmail(email);
    }

    @Override
    public List<User> findBySubscriptionExpirationDate(Date subscriptionExpirationDate) {
        return repository.findUsersBySubscriptionExpirationDate(subscriptionExpirationDate);
    }

    @Override
    public List<User> getAllUsers() {
        return repository.findAll();
    }

    @Override
    public List<User> findByCity(City city) {
        return repository.findUsersByCity(city);
    }
    
    @Override
	public List<User> findByGroup(Group group) {
		return repository.findUsersByGroupsContaining(group);
	}

	@Override
	public List<User> findByEvent(Event event) {
		return repository.findUsersByEventsContaining(event);
	}

    @Override
    public List<User> queryActivity(String keyWord) {
        return repository.findUsersByActivityDescriptionContaining(keyWord);
    }

    @Override
    public List<User> findByFirstNameOrLastName(String firstName, String lastName) {
        return repository.findUsersByFirstNameOrLastName(firstName, lastName);
    }
}

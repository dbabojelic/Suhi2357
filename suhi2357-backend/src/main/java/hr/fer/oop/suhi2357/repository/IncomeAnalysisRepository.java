package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.IncomeAnalysis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IncomeAnalysisRepository extends JpaRepository<IncomeAnalysis, Long> {
    public IncomeAnalysis findByCity(City city);
}

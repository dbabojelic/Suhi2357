package hr.fer.oop.suhi2357.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import hr.fer.oop.suhi2357.form.CompanyForm;
import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.service.ICityService;
import hr.fer.oop.suhi2357.service.ICompanyActivityService;
import hr.fer.oop.suhi2357.service.ICompanyService;
import hr.fer.oop.suhi2357.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    private final ICompanyService companyService;
    private final IUserService userService;
    private final ICompanyActivityService companyActivityService;
    private final ICityService cityService;


	@Autowired
	public CompanyController(final ICompanyService companyService, final IUserService userService, final ICompanyActivityService companyActivityService,
                             final ICityService cityService) {
		this.companyService = companyService;
		this.userService = userService;
		this.companyActivityService = companyActivityService;
		this.cityService = cityService;
	}


    @GetMapping
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getAllCompanies() {
        return ResponseEntity.ok(this.companyService.getAllCompanies());
    }

	@GetMapping("/name/{name}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompaniesWithName(@PathVariable String name) {
    	List<Company> companies = new ArrayList<>();
    	List<String> possibilities = Utils.croatianise(name);
    	
    	for (String posiblity : possibilities) {
    		companies.addAll(companyService.findByName(posiblity));
    	}
    	
    	if (companies == null || companies.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(companies);
    }
	
	@GetMapping("/address/{address}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompaniesWithAddress(@PathVariable String address) {
    	List<Company> companies = new ArrayList<>();
    	List<String> possibilities = Utils.croatianise(address);
    	
    	for (String posibility : possibilities) {
    		companies.addAll(companyService.findByAddress(posibility));
    	}
    	
    	if (companies == null || companies.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(companies);
    }
	
	@GetMapping("/activity/{activityId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompaniesWithActivity(@PathVariable long activityId) {
    	List<Company> companies = companyService.findByActivity(companyActivityService.findById(activityId));
    	
    	if (companies == null || companies.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(companies);
    }

    @GetMapping("/city/{cityId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompaniesWithCity(@PathVariable long cityId) {
	    City city = cityService.findById(cityId);
	    if (city == null) {
	        return ResponseEntity.badRequest().build();
        }
        List<Company> companies = companyService.findByCity(city);

        return ResponseEntity.ok(companies);
    }
	
	@GetMapping("/employee/{employeeId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompaniesWithMember(@PathVariable long employeeId) {
    	List<Company> companies = companyService.findByEmployeesContaining(userService.findById(employeeId));
    	if (companies == null) { 
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(companies.get(0));
    }
	
	@PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> create(@Valid @RequestBody final CompanyForm cf) {
	    if (companyService.findByName(cf.getName()).isEmpty())
	        return ResponseEntity.badRequest().build();
        companyService.updateCompany(new Company(cf.getName(), cf.getAddress(), companyActivityService.findById(cf.getActivityId()), cityService.findById(cf.getCityId())));
       
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
	
	@GetMapping("/remove/{companyId}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
	ResponseEntity<?> remove(@PathVariable long companyId) {
		
		if (companyService.findById(companyId) == null) { 
			return ResponseEntity.badRequest().build();
		}
		
        companyService.removeCompanyById(companyId);

        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
	
	@GetMapping("/companyId/{companyId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompaniesWithId(@PathVariable long companyId) {
    	Company company = companyService.findById(companyId);
    	if (company == null) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(company);
    }
}

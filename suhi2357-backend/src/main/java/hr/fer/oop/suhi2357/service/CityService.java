package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.repository.CityRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService implements ICityService{
	
	@Autowired
	private CityRepository repository;
	
	@Override
	public City findById(Long id) {
		return repository.findOne(id);
	}

	@Override
	public City findByName(String name) {
		return repository.findCityByName(name);
	}

	@Override
	public void updateCity(City city) {
		repository.saveAndFlush(city);
	}

	@Override
	public List<City> getAllCities() {
		return repository.findAll();
	}

	@Override
	public void removeCityById(Long id) {
		repository.delete(id);
	}

}

package hr.fer.oop.suhi2357.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import hr.fer.oop.suhi2357.form.NotificationForm;
import hr.fer.oop.suhi2357.model.Notification;
import hr.fer.oop.suhi2357.model.NotificationType;
import hr.fer.oop.suhi2357.model.Role;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.service.IGroupService;
import hr.fer.oop.suhi2357.service.INotificationService;
import hr.fer.oop.suhi2357.service.IUserService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/notification")
public class NotificationControler {

	private final INotificationService notificationService;
	private final IUserService userService;
	private final IGroupService groupService;
	
	public NotificationControler(INotificationService notificationService,
			IUserService userService, IGroupService groupService) {
		super();
		this.notificationService = notificationService;
		this.userService = userService;
		this.groupService = groupService;
	}

	@GetMapping
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getAllNotifications() {
		return ResponseEntity
				.ok(this.notificationService.getAllNotifications());
	}

	@GetMapping("/notificationId/{notificationId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getNotificationWithId(
			@PathVariable final Long notificationId, Principal principal) {
		String userEmail = principal.getName();
		User u = userService.findByEmail(userEmail);
		Notification notif = notificationService.findById(notificationId);

		if (notif == null)
			return ResponseEntity.badRequest().build();

		List<User> members = notif.getRecievers();
		boolean check = false;

		for (User member : members) {
			if (u.equals(member)) {
				check = true;
			}
		}

		if (!check && !u.equals(notif.getSender()) && u.getRole() == Role.USER)
			return ResponseEntity.badRequest().build();

		return ResponseEntity.ok(notif);
	}

	@PostMapping("/create")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> create(@Valid @RequestBody final NotificationForm nf) {
		List<User> recievers = null;
		NotificationType type = null;
		if (nf.getTypeId() == 1) {
			type = NotificationType.PRIVATE;
			recievers = new ArrayList<>();
			recievers.add(userService.findById(nf.getId()));
		} else if (nf.getTypeId() == 2) {
			type = NotificationType.GROUP;
			recievers = userService.findByGroup(groupService.findById(nf
					.getId()));
		} else if (nf.getTypeId() == 3) {
			type = NotificationType.GLOBAL;
			recievers = userService.getAllUsers();
		} else {
			return ResponseEntity.badRequest().build();
		}
		notificationService.updateNotification(new Notification(
				nf.getMessage(), userService.findById(nf.getSenderId()),
				recievers, type));

		return ResponseEntity.ok(HttpStatus.ACCEPTED);
	}

	@GetMapping("/remove/{notificationId}")
	@PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
	ResponseEntity<?> remove(@PathVariable long notificationId,
			Principal principal) {
		if (notificationService.findById(notificationId) == null) {
			return ResponseEntity.badRequest().build();
		}

		notificationService.removeNotificationById(notificationId);
		return ResponseEntity.ok(HttpStatus.ACCEPTED);
	}

	@GetMapping("/sender/{senderId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getNotificationsSentFrom(@PathVariable long senderId,
			Principal principal) {
		String userEmail = principal.getName();
		User u = userService.findByEmail(userEmail);
		if (u.getId() != senderId && u.getRole() == Role.USER)
			return ResponseEntity.badRequest().build();
		List<Notification> notifications = notificationService
				.findBySender(userService.findById(senderId));
		if (notifications == null) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(notifications);
	}

	@GetMapping("/reciever/{recieverId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getNotificationsRecievedBy(@PathVariable long recieverId,
			Principal principal) {
		String userEmail = principal.getName();
		User u = userService.findByEmail(userEmail);
		if (u.getId() != recieverId && u.getRole() == Role.USER)
			;
		List<Notification> notifications = notificationService
				.findByReciever(userService.findById(recieverId));
		if (notifications == null) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(notifications);
	}
}

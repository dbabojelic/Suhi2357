package hr.fer.oop.suhi2357.form;

public class PathForm {
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

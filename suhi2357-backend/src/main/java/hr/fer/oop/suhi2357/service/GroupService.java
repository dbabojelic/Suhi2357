package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.Group;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupService implements IGroupService {
    @Autowired
    private GroupRepository repository;

    @Override
    public Group findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void updateGroup(Group group) {
        repository.saveAndFlush(group);
    }

    @Override
    public void removeGroupById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Group> findByOwner(User owner) {
        return repository.findGroupsByOwner(owner);
    }

    @Override
    public List<Group> findByMembersContaining(User members) {
        return repository.findGroupsByMembersContaining(members);
    }

    @Override
    public List<Group> findByName(String name) {
        return repository.findGroupsByName(name);
    }

    @Override
    public List<Group> getAllGroups() {
        return repository.findAll();
    }

    @Override
    public void addMemberToGroup(User newMember, Group group) {
        group.getInvited().remove(newMember);
        group.getMembers().add(newMember);
        updateGroup(group);
    }

    @Override
    public void inviteMemberToGroup(User invited, Group group) {
        group.getInvited().add(invited);
        updateGroup(group);
    }

    @Override
    public void exitGroup(User exiter, Group group) {
        group.getInvited().remove(exiter);
        group.getMembers().remove(exiter);
        updateGroup(group);
    }
}

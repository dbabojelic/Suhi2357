package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.Notification;
import hr.fer.oop.suhi2357.model.User;

import java.util.List;

public interface INotificationService {
    public Notification findById(Long id);

    public void updateNotification(Notification notification);

    public void removeNotificationById(Long id);

    public List<Notification> findBySender (User sender);

    public List<Notification> findByReciever(User reciever);

    public List<Notification> getAllNotifications();
}

package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.Group;
import hr.fer.oop.suhi2357.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
	
	public List<Group> findGroupsByOwner (User owner);
	
	public List<Group> findGroupsByMembersContaining (User members);

	public List<Group> findGroupsByName (String name);


}

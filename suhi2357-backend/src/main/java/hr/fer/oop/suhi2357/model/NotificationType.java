package hr.fer.oop.suhi2357.model;

public enum NotificationType {
	PRIVATE, GROUP, GLOBAL;
}

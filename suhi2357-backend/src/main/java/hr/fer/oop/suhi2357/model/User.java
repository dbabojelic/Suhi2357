package hr.fer.oop.suhi2357.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="USERS")
public class User {

    @Id
    @GeneratedValue
    private Long Id;

    @Column(nullable = false, length = 30)
    private String firstName;

    @Column(nullable = false, length = 30)
    private String lastName;

    @Column(name = "password")
    @JsonIgnore
    private String password;

    @ManyToOne
    @JoinColumn(name = "COMPANY")
    private Company company;

    @Column(length = 30)
    private String avatarPath;

    @Column(length = 30)
    private String logoPath;

    @Column(nullable = false)
    private Role role;
    
    @Column(nullable = false, unique = true, length = 30)
    private String email;
    
    @Column(nullable = false, length = 30)
    private String phoneNumber;
    
    @Column(nullable = false)
    private String activityDescription;

    @JsonIgnoreProperties("members")
    @ManyToMany(mappedBy = "members")
    private List<Group> groups;

    @JsonIgnoreProperties("invited")
    @ManyToMany(mappedBy = "invited")
    private List<Group> invitations;

    @JsonIgnore
    @OneToMany(mappedBy = "sender")
    private List<Notification> sentNotifications;

    @JsonIgnore
    @ManyToMany(mappedBy = "recievers")
    private List<Notification> recievedNotifications;
    
    @JsonIgnoreProperties("members")
    @ManyToMany(mappedBy = "members")
    private List<Event> events;
    
    @Column
    private Date subscriptionExpirationDate;
    
    @ManyToOne
    @JoinColumn(name = "CITY")
    private City city;
    
    @Column
    private String address;

    @Column
    private boolean active;
    
    public User(String firstName, String lastName, Company company, Role role, String email, String phoneNumber, String activityDescription, String password, City city, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.role = role;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.activityDescription = activityDescription;
        this.password = new ShaPasswordEncoder(256).encodePassword(password, null);
        subscriptionExpirationDate = new Date(new java.util.Date().getTime() + (long)90*24*60*60*1000);
        this.city = city;
        this.address = address;
        this.active = true;
        this.avatarPath = "";
        this.logoPath = "";
    }

    public User() {}
    
    public City getCity() {
		return city;
	}
    
    public void setCity(City city) {
		this.city = city;
	}
    
    public String getAddress() {
		return address;
	}
    
    public void setAddress(String address) {
		this.address = address;
	}
    
    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new ShaPasswordEncoder(256).encodePassword(password, null);
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Notification> getSentNotifications() {
		return sentNotifications;
	}

	public void setSentNotifications(List<Notification> sentNotifications) {
		this.sentNotifications = sentNotifications;
	}

	public List<Notification> getRecievedNotifications() {
		return recievedNotifications;
	}

	public void setRecievedNotifications(List<Notification> recievedNotifications) {
		this.recievedNotifications = recievedNotifications;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Date getSubscriptionExpirationDate() {
		return subscriptionExpirationDate;
	}

	public void setSubscriptionExpirationDate(Date subscriptionExpirationDate) {
		this.subscriptionExpirationDate = subscriptionExpirationDate;
	}

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public List<Group> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<Group> invitations) {
        this.invitations = invitations;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User))
            return false;
        User u = (User)obj;
        if (u.getId() != this.getId())
            return false;
        return true;
    }
}

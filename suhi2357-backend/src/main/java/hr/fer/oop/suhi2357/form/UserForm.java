package hr.fer.oop.suhi2357.form;

public class UserForm {

    private String firstName;
    private String lastName;
    private String email;
    private long companyId;
    private String phoneNumber;
    private String activity;
    private String password;
    private long cityId;
    private String address;
    
    
    public long getCityId() {
		return cityId;
	}
    
    public void setCityId(long cityId) {
		this.cityId = cityId;
	}
    
    public String getAddress() {
		return address;
	}
    
    public void setAddress(String address) {
		this.address = address;
	}
    
    public String getPassword() {
		return password;
	}
    
    public void setPassword(String password) {
		this.password = password;
	}
    
    public String getActivity() {
		return activity;
	}
    
    public void setActivity(String activity) {
		this.activity = activity;
	}
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

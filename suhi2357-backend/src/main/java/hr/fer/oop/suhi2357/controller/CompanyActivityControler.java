package hr.fer.oop.suhi2357.controller;

import java.util.ArrayList;
import java.util.List;

import hr.fer.oop.suhi2357.form.CompanyActivityForm;
import hr.fer.oop.suhi2357.model.CompanyActivity;
import hr.fer.oop.suhi2357.service.ICompanyActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@RequestMapping("/api/companyactivity")
public class CompanyActivityControler {
	
	private final ICompanyActivityService companyActivityService;

	@Autowired
	public CompanyActivityControler(ICompanyActivityService companyActivityService) {
		this.companyActivityService = companyActivityService;
	}

	@GetMapping
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompanyActivities() {
	    return ResponseEntity.ok(companyActivityService.getAllCompanyActivities());
    }
	
	@GetMapping("/name/{name}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompaniesWithName(@PathVariable String name) {
    	List<CompanyActivity> companyActivities= new ArrayList<>();
    	List<String> possibilities = Utils.croatianise(name);
    	
    	for (String posiblity : possibilities) {
    		companyActivities.addAll(companyActivityService.findActivityByActivityName(posiblity));
    	}
    	if (companyActivities == null || companyActivities.isEmpty()) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(companyActivities);
    }
	
	@GetMapping("/activityId/{activityId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCompanyActivitiesWithId(@PathVariable long activityId) {
    	CompanyActivity companyActivity = companyActivityService.findById(activityId);
    	
    	if (companyActivity == null) {
    		return ResponseEntity.badRequest().build();
    	}
    	
    	return ResponseEntity.ok(companyActivity);
    }
	
	@PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('OWNER')")
    ResponseEntity<?> create(@PathVariable final CompanyActivityForm cf) {
        companyActivityService.updateCompanyActivity(new CompanyActivity(cf.getName()));

		return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

	
	@GetMapping("/remove/{activityId}")
    @PreAuthorize("hasAnyAuthority('OWNER')")
	ResponseEntity<?> remove(@PathVariable long activityId) {
		
		if (companyActivityService.findById(activityId) == null) { 
			return ResponseEntity.badRequest().build();
		}
		
        companyActivityService.removeCompanyActivityById(activityId);

        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
	
	
}

package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.Notification;
import hr.fer.oop.suhi2357.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {
	
	public List<Notification> findNotificationsByMessage (String message);
	
	public List<Notification> findNotificationsBySender (User sender);

	public List<Notification> findNotificationsByRecieversContaining(User reciever);

}

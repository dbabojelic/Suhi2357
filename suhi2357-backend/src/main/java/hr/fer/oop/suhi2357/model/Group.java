package hr.fer.oop.suhi2357.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "GROUPS")
public class Group {

	@Id
	@GeneratedValue
	private long Id;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "OWNER")
	private User owner;
	
	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "Group_User",
	joinColumns = { @JoinColumn(name = "group_Id") },
	inverseJoinColumns = { @JoinColumn(name = "user_Id") })
	private List<User> members;


    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "Group_User_Invite",
    joinColumns = {@JoinColumn(name = "group_Id")},
    inverseJoinColumns = {@JoinColumn(name = "user_Id")})
    private List<User> invited;

	@Column(nullable = false)
	private String name;
	
	public Group () {
	}
	
	public Group(User owner, String name) {
		this.owner = owner;
		this.members = new ArrayList<>();
		this.name = name;
		this.invited = new ArrayList<>();
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public List<User> getMembers() {
		return members;
	}

	public void setMembers(List<User> members) {
		this.members = members;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public List<User> getInvited() {
        return invited;
    }

    public void setInvited(List<User> invited) {
        this.invited = invited;
    }
	
}

package hr.fer.oop.suhi2357.form;

import java.util.List;

public class GroupForm {
	private String name;
	private List<Long> invitedIds;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

    public List<Long> getInvitedIds() {
        return invitedIds;
    }

    public void setInvitedIds(List<Long> invitedIds) {
        this.invitedIds = invitedIds;
    }
}

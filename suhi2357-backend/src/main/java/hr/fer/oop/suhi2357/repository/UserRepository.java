package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
   	public List<User> findUsersByFirstName (String firstName);
	
	public List<User> findUsersByLastName (String lastName);
	
	public List<User> findUsersByCompany (Company company);
	
	public List<User> findUsersByRole (Role role);
	
	public User findUserByEmail (String email);
	
	public List<User> findUsersByGroupsContaining(Group group);
	
	public List<User> findUsersByEventsContaining(Event event);
	

	public List<User> findUsersBySubscriptionExpirationDate (Date subscriptionExpirationDate);
	public List<User> findUsersByCity(City city);
	public List<User> findUsersByActivityDescriptionContaining(String query);
	public List<User> findUsersByFirstNameOrLastName(String firstName, String lastName);

}

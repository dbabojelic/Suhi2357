package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.CompanyActivity;

import java.util.List;

public interface ICompanyActivityService {
    public List<CompanyActivity> getAllCompanyActivities();

    public CompanyActivity findById(Long id);

    public void updateCompanyActivity(CompanyActivity companyActivity);

    public void removeCompanyActivityById(Long id);

    public List<CompanyActivity> findActivityByActivityName (String activityName);
}

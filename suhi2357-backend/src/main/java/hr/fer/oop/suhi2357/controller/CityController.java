package hr.fer.oop.suhi2357.controller;

import java.util.List;

import javax.validation.Valid;

import hr.fer.oop.suhi2357.form.CityForm;
import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.service.ICityService;
import hr.fer.oop.suhi2357.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/city")
public class CityController {

	private final ICityService cityService;
	private final IUserService userService;

	@Autowired
	public CityController(ICityService cityService, IUserService userService) {
		super();
		this.cityService = cityService;
		this.userService = userService;
	}

	@GetMapping("/name/{name}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCitiesWithName(@PathVariable String name) {
		City city = null;
    	List<String> possibilities = Utils.croatianise(name);

    	for (String posiblity : possibilities) {
    		city = cityService.findByName(posiblity);
    		if (city != null) {
    			break;
    		}
    	}

    	if (city == null) {
    		return ResponseEntity.badRequest().build();
    	}

    	return ResponseEntity.ok(city);
    }

	@GetMapping("/cityId/{cityId}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
    ResponseEntity<?> getCitiesWithId(@PathVariable long cityId) {
    	City city = cityService.findById(cityId);
    	if (city == null) {
    		return ResponseEntity.badRequest().build();
    	}

    	return ResponseEntity.ok(city);
    }

	@PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    ResponseEntity<?> create(@Valid @RequestBody final CityForm cf) {
        cityService.updateCity(new City(cf.getName()));

		return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

	@GetMapping("/remove/{cityId}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
	ResponseEntity<?> remove(@PathVariable long cityId) {
		City city = cityService.findById(cityId);
		if (city == null) {
			return ResponseEntity.badRequest().build();
		}

        cityService.removeCityById(cityId);

        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
	

}

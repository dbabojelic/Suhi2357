package hr.fer.oop.suhi2357.repository;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Event;
import hr.fer.oop.suhi2357.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
	
	public List<Event> findEventsByOwner (User owner);
	
	public List<Event> findEventsByMembersContaining (User members);
	
	public List<Event> findEventsByName (String name);
	
	public List<Event> findEventsByPlace (String place);

	public List<Event> findEventsByTime (Date time);

	public List<Event> findEventsByCity(City city);

}

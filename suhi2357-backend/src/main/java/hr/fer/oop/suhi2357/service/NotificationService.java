package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.Notification;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService implements INotificationService {
    @Autowired
    private NotificationRepository repository;

    @Override
    public Notification findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void updateNotification(Notification notification) {
        repository.saveAndFlush(notification);
    }

    @Override
    public void removeNotificationById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<Notification> findBySender(User sender) {
        return repository.findNotificationsBySender(sender);
    }

    @Override
    public List<Notification> findByReciever(User reciever) {
        return repository.findNotificationsByRecieversContaining(reciever);
    }

    @Override
    public List<Notification> getAllNotifications() {
        return repository.findAll();
    }
}

package hr.fer.oop.suhi2357.controller;

import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.Calendar;

@RestController
@RequestMapping("/api/subscription")
public class SubscriptionController {
    private final IUserService userService;

    public SubscriptionController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/extend/{userId}/{noDays}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    ResponseEntity<?> extendSubscription(@PathVariable long userId, @PathVariable int noDays) {
        User user = userService.findById(userId);

        Calendar cal = Calendar.getInstance();
        cal.setTime(user.getSubscriptionExpirationDate());
        cal.add(Calendar.DAY_OF_YEAR, noDays);

        Date newDate = new Date(cal.getTimeInMillis());

        user.setSubscriptionExpirationDate(newDate);
        userService.updateUser(user);

        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }
}

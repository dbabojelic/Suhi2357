package hr.fer.oop.suhi2357.controller;

import hr.fer.oop.suhi2357.form.ChangeUserInformationForm;
import hr.fer.oop.suhi2357.form.PathForm;
import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Role;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.service.ICityService;
import hr.fer.oop.suhi2357.service.ICompanyService;
import hr.fer.oop.suhi2357.service.IEventService;
import hr.fer.oop.suhi2357.service.IGroupService;
import hr.fer.oop.suhi2357.service.IUserService;

import hr.fer.oop.suhi2357.web.ActiveUserStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

	private final IUserService userService;
    private final ICompanyService companyService;
    private final ICityService cityService;
    private final IGroupService groupService;
    private final IEventService eventService;

    @Autowired
    ActiveUserStorage storage;

    @Autowired
    public UserController(final IUserService userService, final ICompanyService companyService, final ICityService cityService, final IGroupService groupService, final IEventService eventService) {
        this.userService = userService;
        this.companyService = companyService;
        this.cityService = cityService;
        this.groupService = groupService;
        this.eventService = eventService;
    }

	@GetMapping
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getAllUsers() {
		return ResponseEntity.ok(this.userService.getAllUsers());
	}

	@GetMapping("/active")
	@PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
	ResponseEntity<?> getAllActiveUsers() {
        List<User> activeUsers = new ArrayList<>();
        for (Long id: this.storage.getActiveUsers())
            activeUsers.add(userService.findById(id));
		return ResponseEntity.ok(activeUsers);
	}

	@GetMapping("/logout")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
    ResponseEntity<?> logoutUser(Principal principal) {
        User u = userService.findByEmail(principal.getName());
        storage.removeUser(u);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

	@GetMapping("/groupId/{groupId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersFromGroupWithId(@PathVariable final Long groupId) {
		List<User> members = userService.findByGroup(groupService
				.findById(groupId));
		if (members == null)
			return ResponseEntity.badRequest().build();

		return ResponseEntity.ok(members);
	}
	
	@GetMapping("/eventId/{eventId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersFromEventWithId(@PathVariable final Long eventId) {
		List<User> members = userService.findByEvent(eventService.findById(eventId));
		
		if (members == null)
			return ResponseEntity.badRequest().build();

		return ResponseEntity.ok(members);
	}

	@GetMapping("/userId/{userId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUserWithId(@PathVariable final Long userId) {
		User u = userService.findById(userId);
		if (u == null)
			return ResponseEntity.badRequest().build();

		return ResponseEntity.ok(u);
	}

	@GetMapping("/firstName/{firstName}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersWithFirstName(@PathVariable String firstName) {
		List<User> users = new ArrayList<>();
		List<String> possibilities = Utils.croatianise(firstName);

		for (String posibility : possibilities) {
			users.addAll(userService.findByFirstName(posibility));
		}

		if (users == null || users.isEmpty()) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(users);
	}

	@GetMapping("/lastName/{lastName}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersWithLastName(@PathVariable String lastName) {
		List<User> users = new ArrayList<>();
		List<String> possibilities = Utils.croatianise(lastName);

		for (String posibility : possibilities) {
			users.addAll(userService.findByLastName(posibility));
		}
		if (users == null || users.isEmpty()) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(users);
	}

	@GetMapping("/company/{companyId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersInCompany(@PathVariable final long companyId) {
		List<User> users = userService.findByCompany(companyService
				.findById(companyId));
		if (users == null || users.isEmpty()) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(users);
	}

	@GetMapping("/email/{email}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersWithEmail(@PathVariable String email) {
		User users = userService.findByEmail(email);
		if (users == null) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(users);
	}

	@GetMapping("/role/{roleId}")
	@PreAuthorize("hasAnyAuthority('ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersWithRole(@PathVariable int roleId) {
		Role role;
		if (roleId == 1) {
			role = Role.USER;
		} else if (roleId == 2) {
			role = Role.ADMIN;
		} else if (roleId == 3) {
			role = Role.OWNER;
		} else {
			return ResponseEntity.badRequest().build();
		}

		List<User> users = userService.findByRole(role);
		if (users == null || users.isEmpty()) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(users);
	}

	@PostMapping("/change")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> register(
			@RequestBody final ChangeUserInformationForm cuif,
			Principal principal) {
		User user = userService.findById(cuif.getUserId());

		if (!cuif.getEmail().equals(user.getEmail())) {
			if (userService.findByEmail(cuif.getEmail()) != null) {
				return ResponseEntity.badRequest().build();
			}
		}

		if (!user.equals(userService.findByEmail(principal.getName()))
				&& userService.findByEmail(principal.getName()).getRole() == Role.USER)
			return ResponseEntity.badRequest().build();

		user.setEmail(cuif.getEmail());
		user.setActivityDescription(cuif.getActivity());
		user.setCompany(companyService.findById(cuif.getCompanyId()));
		user.setFirstName(cuif.getFirstName());
		user.setLastName(cuif.getLastName());
		user.setPhoneNumber(cuif.getPhoneNumber());
		user.setCity(cityService.findById(cuif.getCityId()));
		user.setAddress(cuif.getAddress());

		userService.updateUser(user);

		return ResponseEntity.ok(HttpStatus.ACCEPTED);
	}

	@GetMapping("/city/{cityId}")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getUsersWithCity(@PathVariable long cityId) {
		City city = cityService.findById(cityId);
		if (city == null) {
			return ResponseEntity.badRequest().build();
		}
		List<User> users = userService.findByCity(cityService.findById(cityId));

		return ResponseEntity.ok(users);
	}

	@GetMapping("/image/avatar")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getAvatar(Principal principal) {
		PathForm form = new PathForm();
		form.setPath(userService.findByEmail(principal.getName()).getAvatarPath());
		return ResponseEntity.ok(form);
	}

	@GetMapping("/image/logo")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> getLogo(Principal principal) {
		PathForm form = new PathForm();
		form.setPath(userService.findByEmail(principal.getName()).getLogoPath());
		return ResponseEntity.ok(form);
	}

	@PostMapping("/search/name")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> searchName(@RequestBody String name) {
    	String[] splitName = name.split("\\s+");
    	String firstName = splitName[0];
    	String lastName = "";
    	System.out.println(name);
    	if (splitName.length > 1) {
			lastName = splitName[1];
			return ResponseEntity.ok(userService.findByFirstNameOrLastName(firstName, lastName));
		}
		List<User> users = userService.findByFirstName(firstName);
    	for (User u: userService.findByLastName(firstName))
    		if (!users.contains(u))
    			users.add(u);
    	return ResponseEntity.ok(users);
	}

	@PostMapping("/search/activity")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> searchActivity(@RequestBody String activity) {
    	String[] tokens = activity.split("\\s+");
		List<User> users = userService.queryActivity(tokens[0]);
		for (int i = 1; i < tokens.length; i++) {
			for (User u: userService.queryActivity(tokens[i])) {
				if (!users.contains(u))
					users.add(u);
			}
		}
    	return ResponseEntity.ok(users);
	}

	@GetMapping("/whoami")
	@PreAuthorize("hasAnyAuthority('USER', 'ADMIN', 'OWNER')")
	ResponseEntity<?> whoAmI(Principal principal) {
    	return ResponseEntity.ok(userService.findByEmail(principal.getName()));
	}


}

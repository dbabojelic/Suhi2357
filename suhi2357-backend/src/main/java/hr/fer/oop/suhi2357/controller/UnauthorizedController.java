package hr.fer.oop.suhi2357.controller;

import hr.fer.oop.suhi2357.form.UserForm;
import hr.fer.oop.suhi2357.mail.EmailService;
import hr.fer.oop.suhi2357.model.IncomeAnalysis;
import hr.fer.oop.suhi2357.model.Role;
import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.sql.Date;
import java.util.List;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/unauthorized")
public class UnauthorizedController {


    private final IUserService userService;
    private final ICompanyService companyService;
    private final ICityService cityService;

    @Autowired
    private EmailService emailService;
    @Autowired
    private IIncomeAnalysisService incomeAnalysisService;

    @Autowired
    public UnauthorizedController(final IUserService userService, final ICompanyService companyService, ICityService cityService) {
        this.userService = userService;
        this.companyService = companyService;
        this.cityService = cityService;
    }

    @PostMapping("/register")
    ResponseEntity<?> register(@RequestBody final UserForm uf) {
        if (userService.findByEmail(uf.getEmail()) != null)
            return ResponseEntity.badRequest().build();

        userService.updateUser(new User(uf.getFirstName(), uf.getLastName(), companyService.findById(uf.getCompanyId()), Role.USER, uf.getEmail(),
                uf.getPhoneNumber(), uf.getActivity(), uf.getPassword(), cityService.findById(uf.getCityId()), uf.getAddress()));

        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @GetMapping("/companies")
    ResponseEntity<?> getAllCompanies() {
        return ResponseEntity.ok(this.companyService.getAllCompanies());
    }

    @GetMapping("/city")
    ResponseEntity<?> getAllCities() {
        return ResponseEntity.ok(this.cityService.getAllCities());
    }

    @PostMapping(value = "/image/avatar/{userId}", consumes = "multipart/form-data")
    ResponseEntity<?> postAvatarImage(@PathVariable long userId, @RequestParam("file") @RequestBody MultipartFile file) {
        try {

            byte[] bytes = file.getBytes();

            // Creating the directory to store file
            final String dir2 = System.getProperty("user.dir");
            File dir = new File(dir2 + File.separator + "src/main/resources/static");
            if (!dir.exists())
                dir.mkdirs();

            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()
                    + File.separator + "image-avatar" + userId + ".png");
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();
            User user = userService.findById(userId);
            user.setAvatarPath(File.separator + "image-avatar" + userId + ".png");
            userService.updateUser(user);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/image/logo/{userId}", consumes = "multipart/form-data")
    ResponseEntity<?> postLogoImage(@PathVariable long userId, @RequestParam("file") @RequestBody MultipartFile file) {
        try {

            byte[] bytes = file.getBytes();

            // Creating the directory to store file
            final String dir2 = System.getProperty("user.dir");
            File dir = new File(dir2 + File.separator + "src/main/resources/static");
            if (!dir.exists())
                dir.mkdirs();

            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()
                    + File.separator + "image-logo" + userId + ".png");
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();
            User user = userService.findById(userId);
            user.setAvatarPath(File.separator + "image-logo" + userId + ".png");
            userService.updateUser(user);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("/checksubs")
    ResponseEntity<?> checkSubsriptions() {
        List<User> users = userService.getAllUsers();
        Date now = new Date(new java.util.Date().getTime());
        for (User user : users) {
            Date expires = user.getSubscriptionExpirationDate();
            if (new Date(expires.getTime() - (long)5*24*60*60*1000).before(now)) {
                emailService.sendMessage(user.getEmail(),
                        "Suhi2357 subscription expires in 5 days",
                        "Hello,\n\\nYour Suhi2357 subscription expires in 5 days, please" +
                                " renew your subscription or contact Suhi2357's " +
                                "administrators for further help.\n\nRegards,\nSuhi2357");
            }
            else if (new Date(expires.getTime() - (long)2*24*60*60*1000).before(now)) {
                emailService.sendMessage(user.getEmail(),
                        "Suhi2357 subscription expires in 2 days",
                        "Hello,\n\\nYour Suhi2357 subscription expires in 2 days, please" +
                                "renew your subscription or contact Suhi2357's " +
                                "administrators for further help.\n\nRegards,\nSuhi2357");
            }
            else if (expires.before(now)) {
                user.setActive(false);
                userService.updateUser(user);
                emailService.sendMessage(user.getEmail(),
                        "Suhi2357 subscription expired",
                        "Hello,\n\\nYour Suhi2357 subscription expired and your account" +
                                "has been set to inactive state, please contact Suhi2357's " +
                                "administrators for further help.\n\nRegards,\nSuhi2357");
            }
        }
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @GetMapping("/income")
    ResponseEntity<?> income(@RequestParam("cityName") @RequestBody String cityName) {
        incomeAnalysisService.addNewVisit(cityName);
        return ResponseEntity.ok().build();
    }

}

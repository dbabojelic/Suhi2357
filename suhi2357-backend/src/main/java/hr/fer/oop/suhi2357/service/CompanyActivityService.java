package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.CompanyActivity;
import hr.fer.oop.suhi2357.repository.CompanyActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyActivityService implements ICompanyActivityService {
    @Autowired
    private CompanyActivityRepository repository;

    @Override
    public CompanyActivity findById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void updateCompanyActivity(CompanyActivity companyActivity) {
        repository.saveAndFlush(companyActivity);
    }

    @Override
    public void removeCompanyActivityById(Long id) {
        repository.delete(id);
    }

    @Override
    public List<CompanyActivity> findActivityByActivityName(String activityName) {
        return repository.findCompanyActivityByActivityName(activityName);
    }

    @Override
    public List<CompanyActivity> getAllCompanyActivities() {
        return repository.findAll();
    }
}

package hr.fer.oop.suhi2357.model;

import javax.persistence.*;

@Entity
@Table(name="CITIES")
public class City {
	
	@Id
	@GeneratedValue
	private long Id;
	
	@Column(nullable = false, length = 30, unique = true)
	private String name;
	
	public City() {
	}
	
	public City(String name) {
		this.name = name;
	}

	public long getId() {
		return Id;
	}
	
	public void setId(long id) {
		Id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}

package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.User;
import hr.fer.oop.suhi2357.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findUserByEmail(s);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().name()));
//        user.getRoles().forEach(role -> {
//            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
//        });
        UserDetails userDetails;
        if( user.isActive() ) {
            userDetails = new org.springframework.security.core.userdetails.
                    User(user.getEmail(), user.getPassword(), authorities);
        }
        else {
            userDetails = new org.springframework.security.core.userdetails.
                    User(user.getEmail(),
                    new ShaPasswordEncoder(256)
                            .encodePassword(user.getPassword(), null),
                    authorities);
        }

        return userDetails;
    }
}

package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.City;
import hr.fer.oop.suhi2357.model.Company;
import hr.fer.oop.suhi2357.model.Event;
import hr.fer.oop.suhi2357.model.Group;
import hr.fer.oop.suhi2357.model.Role;
import hr.fer.oop.suhi2357.model.User;

import java.util.Date;
import java.util.List;

public interface IUserService {
    public User findById(Long id);

    public void updateUser(User user);

    public void removeUserById(Long id);

    public List<User> findByFirstName (String firstName);

    public List<User> findByLastName (String lastName);

    public List<User> findByCompany (Company company);

    public List<User> findByRole (Role role);

    public User findByEmail (String email);

    public List<User> findBySubscriptionExpirationDate (Date subscriptionExpirationDate);

    public List<User> getAllUsers();

    public List<User> findByCity(City city);
    
    public List<User> findByGroup(Group group);
    
    public List<User> findByEvent(Event event);

    public List<User> queryActivity(String keyWord);

    public List<User> findByFirstNameOrLastName(String firstName, String lastName);
}

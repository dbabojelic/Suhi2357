package hr.fer.oop.suhi2357.service;

import hr.fer.oop.suhi2357.model.IncomeAnalysis;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IIncomeAnalysisService {
    public List<IncomeAnalysis> getAnalysis();
    public void addNewVisit(String cityName);
    public void updateIncomeAnalysis(IncomeAnalysis analysis);
}

import React, {Component} from 'react';
import './SearchBar.css';

class SearchBar extends Component {
  search() {
	  let input, filter, table, tr, td, i;
	  input = document.getElementById("myInput");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("myTable");
	  tr = table.getElementsByTagName("tr");

	  for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[0];
		if (td) {
		  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
			tr[i].style.display = "";
		  } else {
			tr[i].style.display = "none";
		  }
		}
	  }
	}
  render() {
    return (
      <div className="trazilica">
				<h2>Pretraži članove</h2>
				<input className="trazinput" type="text" id="myInput" onKeyUp={() => this.search()} placeholder="Tražilica" title="Type in a name"/>
				<table id="myTable">
				  <tr>
					<td>Zvonimir</td>
				  </tr>
				  <tr>
					<td>Marko</td>
				  </tr>
				  <tr>
					<td>Luka</td>
				  </tr>
				  <tr>
					<td>Dario</td>
				  </tr>
				  <tr>
					<td>Vjeko</td>
				  </tr>
				  <tr>
					<td>Ivan</td>
				  </tr>
				</table>
			</div>
    );
  }
}

export default SearchBar;

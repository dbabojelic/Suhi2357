import React, {Component} from 'react';
import './Accordion.css';

import {fetchData} from "../../constants/communication";
import paths from '../../constants/paths';
import {read} from '../../services/storage';
import {Link} from 'react-router';

class Accordion extends Component {
  // onAccClick(){
	// 	let acc = document.getElementsByClassName("accordion");
  //   console.log(acc)
	// 	let i;
	// 	for (i = 0; i < acc.length; i++) {
	// 	  acc[i].addEventListener("click", function() {
	// 		this.classList.toggle("active");
	// 		let panel = this.nextElementSibling;
	// 		if (panel.style.maxHeight){
	// 		  panel.style.maxHeight = null;
	// 		} else {
	// 		  panel.style.maxHeight = panel.scrollHeight + "px";
	// 		}
	// 	  });
	// 	}
	// }
  potvrdiGroup(gId) {
    ;
  }
  render() {
    return (
      <div className="accrd">
        <div>
          <button class="accordion">Moje grupe...</button>

          <div class="panel">
            <ul>
              {
                this.props.invitedGroups.map((g) => <li>{g.name}  -> <a onClick={() => fetchData(paths.restApi.acceptInviteToGroup(g.id), read('token'))}>Potvrdi</a></li>)
              }
              {
                this.props.memberGroups.map((g) => <li><Link to={`group/${g.id}`}>{g.name}</Link></li>)
              }
            </ul>

          </div>
        </div>
        <div>
          <button class="accordion">Moji događaji...</button>
          <div class="panel">
            <ul>
              {
                this.props.events.map((g) => <li><Link to={`event/${g.id}`}>{g.name}</Link></li>)
              }
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Accordion;

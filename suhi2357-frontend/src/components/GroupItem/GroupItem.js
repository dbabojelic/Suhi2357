import React from 'react';
import './GroupItem.css';

import {browserHistory} from 'react-router';
import {fetchData} from "../../constants/communication";
import paths from '../../constants/paths';
import {read} from '../../services/storage';

export default ({group}) => (
  <li onClick={() => browserHistory.push(`group/${group.id}`)}>{group.name}  -> <a onClick={() => fetchData(paths.restApi.acceptInviteToGroup(group.id), read('token'))}>Potvrdi</a></li>
);

// onClick={() => browserHistory.push(`pokemon-details/${pokemon.id}`)}

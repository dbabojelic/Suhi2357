import React, {Component} from 'react';
import './Announcement.css';

class Announcement extends Component {
  openModal() {
    document.getElementById("myModal").style.display = "block";
  }
  closeModal() {
    document.getElementById("myModal").style.display = "none";
  }
  render() {
    return (
      <div>
        <div onClick={() => this.openModal()} className="objava">
          <p className="objava-header">Vrsta: {this.props.notif.type}</p>
          <p className="objava-header">Od: {this.props.notif.sender.firstName.concat(" ").concat(this.props.notif.sender.lastName)}</p><br></br>
          <p>{this.props.notif.message}</p><br></br>
  			</div>
        <hr/>
        <div id="myModal" className="modal">
          <div className="modal-content">
            <span onClick={() => this.closeModal()} className="close">&times;</span>
            <p>Tu se vide detalji objave kao tekst, objavitelj itd...........</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Announcement;

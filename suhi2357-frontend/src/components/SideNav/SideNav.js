import React, {Component} from 'react';
// import logoURL from '../../assets/logo.png';
import './SideNav.css';
import {fetchData, postData, fetchUnauthCompaines} from '../../constants/communication';
import paths from '../../constants/paths';
import {read} from '../../services/storage';
import {browserHistory} from 'react-router';

class SideNav extends Component {
  constructor(args) {
    super(args);
    this.state = {
      users: [],
      invitedGroupUsers: [],
      groupInvCount: 0,
      invitedEventUsers: [],
      eventInvCount: 0,
      gradovi: [],
      me: {}
    };
    this.handleGroupSubmit = this.handleGroupSubmit.bind(this);
    this.handleEventSubmit = this.handleEventSubmit.bind(this);
  }
  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    // document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
  }
  closeModal(modNum) {
    switch (modNum) {
      case "1":
      document.getElementById("myModal1").style.display = "none";
      break;
      case "2":
      document.getElementById("myModal2").style.display = "none";
      break;
      default:
      return;
    }
    this.openNav();
  }
  /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";

    // document.body.style.backgroundColor = "white";
  }
  modal(modNum) {
    this.closeNav();
    switch (modNum) {
      case "1":
      document.getElementById("myModal1").style.display = "block";
      break;
      case "2":
      document.getElementById("myModal2").style.display = "block";
      break;
      default:
      return;
    }
  }
  getStuff() {
    fetchData(paths.restApi.fetchWhoAmI, read('token'))
    .then((res) => {
      this.setState({
        me: res
      });
    });
    fetchData(paths.restApi.fetchUsers, read('token'))
    .then((res) => {
      this.setState({
        users: res
      });
    });
    fetchUnauthCompaines(paths.unauthorized.city)
    .then((res) => {
      this.setState({
        gradovi: res
      });
    });
  }
  componentDidMount() {
    this.getStuff();
  }
  pozoviGrupa() {
    let inputUser = document.getElementById("input1").value.split(" ");
    let userId = this.state.users.filter((k) => k.firstName === inputUser[0] && k.lastName === inputUser[1])[0].id;
    let inp = document.getElementById("input1");
    let ta = document.getElementById("pozvaniGrupa");
    let prefiks = this.state.groupInvCount === 0 ? "" : ", ";
    ta.value += prefiks.concat(inp.value)
    this.state.invitedGroupUsers.push(userId);
    this.setState({
      groupInvCount: 1
    });
    inp.value="";
  }
  handleGroupSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    // let inputGrad = document.getElementById("input2").value;
    // let gradId = this.state.cities.filter((grad) => grad.name === inputGrad)[0].id;
    // data.append('cityId', gradId);
    // data.append('activity', "");
    // data.append('companyId', 1);
    // data.delete('password2');
    console.log("PODACI:");
    new Response(data).text().then(console.log);
  }
  pozoviEvent() {
    let inputUser = document.getElementById("input2").value.split(" ");
    let userId = this.state.users.filter((k) => k.firstName === inputUser[0] && k.lastName === inputUser[1])[0].id;
    let inp = document.getElementById("input2");
    let ta = document.getElementById("pozvaniEvent");
    let prefiks = this.state.eventInvCount === 0 ? "" : ", ";
    ta.value += prefiks.concat(inp.value)
    this.state.invitedEventUsers.push(userId);
    this.setState({
      eventInvCount: 1
    });
    inp.value="";
  }
  handleEventSubmit(event) {
    event.preventDefault();
    const data = {};
    let inputGrad = document.getElementById("i2").value;
    console.log(inputGrad);
    let gradId = this.state.gradovi.filter((grad) => grad.name === inputGrad)[0].id;
    let nazivVal = document.getElementById("i1").value;
    let mjestoVal = document.getElementById("i3").value;
    let vrijemeVal = document.getElementById("i4").value;
    data['name'] = nazivVal;
    data['place'] = mjestoVal;
    data['cityId'] = gradId;
    data['time'] = vrijemeVal;
    console.log(data);
    // data.append('cityId', gradId);
    // data.append('activity', "");
    // data.append('companyId', 1);
    // data.delete('password2');
    console.log("PODACI:");
    new Response(data).text().then(console.log);
    postData(paths.restApi.createEvent, read('token'), data)
    .then((res) => {
      document.getElementById("pozvaniEvent").value = "";
      this.setState({
        eventInvCount: 0
      });
      this.closeModal("2");
      alert("Uspješno stvoren događaj!");
  });
  }
  stvoriEvent() {

  }
  stvoriGrupu() {
    let inputUser = document.getElementById("grupaIme").value;
    let data = {
      "name" : inputUser,
      "invitedIds" : this.state.invitedGroupUsers
    }
    postData(paths.restApi.createGroup, read('token'), data)
    .then((res) => {
      document.getElementById("pozvaniGrupa").value = "";
      this.setState({
        groupInvCount: 0
      });
      this.closeModal("1");
      alert("Uspješno stvorena grupa!");
  });
}
  render() {
    return (
      <div>
        <div id="mySidenav" className="sidenav">
          <a href="javascript:void(0)" className="closebtn" onClick={() => this.closeNav()}>&times;</a>
          <a onClick={() => this.modal("1")}> Nova grupa</a>
          <a onClick={() => this.modal("2")}> Novi događaj</a>
          {/* <a href="/reports">Vidi analize korisnika</a> */}
          {
            this.state.me.role === "USER" ?
            null
            :
            <div>
              <a href="/traffic">Administratorske mogućnosti</a>
              <a href="/feesview">Vidi članarine</a>
            </div>
          }
        </div>
        <div id="main">
          <div id="myModal1" className="modal">
            <form onSubmit={this.handleGroupSubmit}>
              <div className="modal-content">
                <span onClick={() => this.closeModal("1")} className="close">&times;</span>
                <p>Nova grupa</p>
                <input id="grupaIme" placeholder="Naziv grupe"/>
                <div className="invite">
                  <div className="inv1">
                    <p>Dodaj člana u grupu:</p>
                    <input placeholder="ime korisnika" list="users" id="input1"/>
                    <datalist id="users">
                      {this.state.users.map((item) =>
                        <option value={item.firstName.concat(" ").concat(item.lastName)}/>
                      )}
                    </datalist>
                  </div>
                  <button onClick={() => this.pozoviGrupa()}>Pozovi</button>
                  <div className="inv2">
                    <p>Pozvane osobe</p>
                    <textarea id="pozvaniGrupa" rows="2" cols="50"></textarea>
                  </div>
                  <button onClick={() => this.stvoriGrupu()}>Stvori</button>
                </div>
              </div>
            </form>
          </div>
          <div id="myModal2" className="modal">
            <form onSubmit={this.handleEventSubmit}>
              <div className="modal-content">
                <span onClick={() => this.closeModal("2")} className="close">&times;</span>
                <p>Novi događaj</p>
                <input id="i1" placeholder="Naziv eventa"/>
                <input id="i2" placeholder="odaberi grad" list="cities" required/>
                <datalist id="cities">
                  {this.state.gradovi.map((item) =>
                    <option value={item.name}/>
                  )}
                </datalist>
                <input id="i3" placeholder="Mjesto"/>
                <input id="i4" placeholder="Vrijeme" type="date"/>
                <div className="invite">
                  <div className="inv1">
                    <p>Pošalji pozivnicu članu:</p>
                    <input placeholder="ime korisnika" list="users2" id="input2"/>
                    <datalist id="users2">
                      {this.state.users.map((item) =>
                        <option value={item.firstName.concat(" ").concat(item.lastName)}/>
                      )}
                    </datalist>
                  </div>
                  <button onClick={() => this.pozoviEvent()}>Pozovi</button>
                  <div className="inv2">
                    <p>Pozvane osobe</p>
                    <textarea id="pozvaniEvent" rows="2" cols="50"></textarea>
                  </div>
                  <button>Podnesi</button>

                </div>
              </div>
            </form>
          </div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default SideNav;

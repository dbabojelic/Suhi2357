import React, {Component} from 'react';
import logoURL from '../../assets/logo.PNG';
import hamburgerIcon from '../../assets/Hamburger_icon.png';
import './Header.css';
import {browserHistory} from 'react-router';
import SideNav from '../SideNav/SideNav';

class Header extends Component {
  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    // document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
  }
  render() {
    return (
      <div>
        <SideNav>
          <div className="head-cont">
            <div className="authorized-container__header">
              <div className="head-pics">
                <img className="hicon" onClick={() => this.openNav()} src={hamburgerIcon}/>
                <a href="/home"><img className="authorized-container__logo" src={logoURL} /></a>
              </div>
              <div className="authorized-container__navigation">
                <a href="/home" className="authorized-container__navigation-link">Naslovnica</a>
                <a href="/changeinfo" className="authorized-container__navigation-link">Moj profil</a>
                <a href="/" className="authorized-container__navigation-link">Odjavi se</a>
              </div>
            </div>
            {this.props.children}
          </div>
        </SideNav>
      </div>
    );
  }
}

export default Header;

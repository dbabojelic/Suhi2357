
const paths = {
    restApi: {

        fetchCompanyActivities: "/api/companyactivity",
        fetchCompanyActivityByName: (name) => {
            return "/api/companyactivity/name/" + name
        },
        fetchCompanyActivityById: (id) => {
            return "/api/companyactivity/activityId/" + id
        },
        createCompanyActivity: "/api/companyactivity/create",
        removeCompanyActivityById: (id) => {
            return "/api/companyactivity/remove/" + id
        },

        fetchCompanies: "/api/company",
        fetchCompanyByName: (name) => {
            return "/api/company/name/" + name
        },
        fetchCompanyById: (id) => {
            return "/api/company/companyId/" + id
        },
        fetchCompaniesByActivity: (activity) => {
            return "/api/company/activity/" + activity
        },
        fetchCompaniesByEmployeeId: (employeeId) => {
            return "/api/company/employee/" + employeeId
        },
        fetchCompaniesByAddress: (address) => {
            return "/api/company/address/" + address
        },
        removeCompanyById: (id) => {
            return "/api/company/remove/" + id
        },
        createCompany: "/api/company/create",
        fetchCompaniesByCityId: (id) => {
            return "/api/company/city/" + id
        },

        fetchEvents: "/api/event",
        fetchEventById: (id) => {
            return "/api/event/eventId/" + id
        },
        createEvent: "/api/event/create",
        removeEventById: (id) => {
            return "/api/event/remove/" + id
        },
        fetchEventsByOwnerId: (id) => {
            return "/api/event/owner/" + id
        },
        fetchEventsByMemberId: (id) => {
            return "/api/event/member/" + id
        },
        addMemberToEvent: (memberId, eventId) => {
            return "/api/event/member/" + memberId + "/" + eventId
        },
        fetchEventsByName: (name) => {
            return "/api/event/name/" + name
        },
        fetchEventsByPlace: (place) => {
            return "/api/event/place/" + place
        },
        fetchEventsByCityId: (id) => {
            return "/api/event/city/" + id
        },

        fetchGroups: "/api/group",
        fetchGroupsByName: (name) => {
            return "/api/group/name/" + name
        },
        fetchGroupsByOwnerId: (id) => {
            return "/api/group/owner/" + id
        },
        fetchGroupById: (id) => {
            return "/api/group/groupId/" + id
        },
        fetchGroupsByMemberId: (id) => {
            return "/api/group/member/" + id
        },
        fetchGroupsByInvitedId: (id) => {
            return "/api/group/invited/" + id
        },
        addNewMemberToGroup: (newMemberId, groupId) => {
            return "/api/group/member/add/" + newMemberId + "/" + groupId
        },
        createGroup: "/api/group/create",
        removeGroupById: (id) => {
            "/api/group/remove/" + id
        },
        inviteMemberToGroup: (newMemberId, groupId) => {
            return "/api/group/member/invite/" + newMemberId + "/" + groupId
        },
        acceptInviteToGroup: (groupId) => {
            return "/api/group/member/invite/accept/" + groupId
        },
        exitGroup: (groupId) => {
            return "/api/group/member/exit/" + groupId
        },

        fetchNotifications: "/api/notification",
        fetchNotificationById: (id) => {
            return "/api/notification/notificationId/" + id
        },
        createNotification: "/api/notification/create",
        removeNotificationById: (id) => {
            return "/api/notification/remove/" + id
        },
        fetchNotificationsBySenderId: (id) => {
            return "/api/notification/sender/" + id
        },
        fetchNotificationsByRecieverId: (id) => {
            return "/api/notification/reciever/" + id
        },

        fetchUsers: "/api/user",
        fetchLoggedInUsers: "/api/user/active",
        logoutUser: "/api/user/logout", //bitno mi je za statistiku trenutno ulogiranih
        fetchUserById: (id) => {
            return "/api/user/userId/" + id
        },
        fetchUsersByFirstName: (name) => {
            return "/api/user/firstName/" + name
        },
        fetchUsersByLastName: (name) => {
            return "/api/user/lastName/" + name
        },
        fetchUsersByCompanyId: (id) => {
            return "/api/user/company/" + id
        },
        fetchUserByEmail: (email) => {
            return "/api/user/email/" + email
        },
        fetchUsersByRoleId: (id) => {
            return "/api/user/role/" + id
        },
        fetchUsersByCityId: (id) => {
            return "/api/user/city/" + id
        },
        searchUsersByName: "/api/user/search/name",
        searchUsersByActivity: "/api/user/search/activity",
        fetchWhoAmI: "/api/user/whoami",
        fetchPathToUserAvatar: "/api/user/image/avatar",
        fetchPathToUserLogo: "/api/user/image/logo",
        fetchUnregisteredAnalysis: "/api/analysis/unregistered",
        fetchRegisteredAnalysis: "/api/analysis/registered",
    },



    unauthorized: {
        register: "/unauthorized/register",
        login: "/oauth/token",
        companies: "/unauthorized/companies",
        city: "/unauthorized/city",
        markNewIncome: "/unauthorized/income",
    }



};

export default paths;

import paths from './paths';

let BACKEND = "http://138.68.92.193:8080";
// let BACKEND = "http://localhost:8080";


function fetchLoginToken(username, password) {
    const body = `username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}&grant_type=password`;
    return  fetch(BACKEND + paths.unauthorized.login, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + btoa('testclientid:XY7kmzoNzl100'),
        },
        body: body
    }).then((res) => res.json());
}

function fetchData(path, token) {
    return fetch(BACKEND + path, {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token
        },
    }).then((res) => res.json());
}

function fetchSlika(path, token) {
    return fetch(BACKEND + path, {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token
        },
    }).then((res) => res.json());
}

function register(userData) {
    return fetch(BACKEND + paths.unauthorized.register, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(userData),
    }).then((res) => res.json());
}

function postData(path, token, data) {
    return fetch(BACKEND + path, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify(data),
    }).then((res) => res.json());
}

function fetchUnauthCompaines(path) {
  return fetch(BACKEND + path, {
      method: 'GET',
      headers: {},
  }).then((res) => res.json());
}

export {fetchLoginToken};
export {fetchData};
export {register};
export {postData};
export {fetchUnauthCompaines};
export {fetchSlika};

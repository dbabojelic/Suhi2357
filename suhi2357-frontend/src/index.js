// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';
// ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();

//////////////////////////////

import './index.css';
import React from 'react';

import App from './App';
import Home from './pages/Home/Home';
import Registration from './pages/Registration/Registration';
import LandingPage from './pages/LandingPage/LandingPage';
import UsersView from './pages/UsersView/UsersView';
import GroupView from './pages/GroupView/GroupView';
import EventView from './pages/EventView/EventView';
import Reports from './pages/Reports/Reports';
import Traffic from './pages/Traffic/Traffic';
import NewGroup from './pages/NewGroup/NewGroup';
import NewEvent from './pages/NewEvent/NewEvent';
import FeesView from './pages/FeesView/FeesView';
import ChangeInfo from './pages/ChangeInfo/ChangeInfo';
import SendMessage from './pages/SendMessage/SendMessage';
import Error from './pages/Error/Error';
import Header from './components/Header/Header';
import UserDetails from './pages/UserDetails/UserDetails';
import GroupDetails from './pages/GroupDetails/GroupDetails';
import EventDetails from './pages/EventDetails/EventDetails';

import {render} from 'react-dom';
import {useStrict} from 'mobx';
import {Router, Route, browserHistory, IndexRoute, hashHistory} from 'react-router';

useStrict(true);

render(
  <Router history={browserHistory}>
    <Route path="/" component={App} />
    <Route path="/registration" component={Registration} />
    <Route path="/home" component={Header}>
      <IndexRoute component={Home}/>
      <Route path="/changeinfo" component={ChangeInfo} />
      <Route path="/usersview" component={UsersView} />
      <Route path="/groupview" component={GroupView} />
      <Route path="/eventview" component={EventView} />
      <Route path="/reports" component={Reports} />
      <Route path="/traffic" component={Traffic} />
      <Route path="/newgroup" component={NewGroup} />
      <Route path="/newevent" component={NewEvent} />
      <Route path="/feesview" component={FeesView} />
      <Route path="/sendmessage" component={SendMessage} />
      <Route path="/error" component={Error} />
      <Route path="/user/:id" component={UserDetails} />
      <Route path="/event/:id" component={EventDetails} />
      <Route path="/group/:id" component={GroupDetails} />
    </Route>
  </Router>
  , document.getElementById('root'));

import React, { Component } from 'react';
import './EventView.css';
import {Link} from 'react-router';

class EventView extends Component {
  render() {
    return (
      <div className="EventView">
        <h1>Vidi događaj</h1>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default EventView;

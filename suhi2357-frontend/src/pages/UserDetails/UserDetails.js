import React, { Component } from 'react';
import './UserDetails.css';
import {Link} from 'react-router';

class UserDetails extends Component {
  render() {
    return (
      <div className="EventView">
        <h1>Vidi događaj</h1>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default UserDetails;

import React, { Component } from 'react';
import './Traffic.css';
import {Link} from 'react-router';

class Traffic extends Component {
	myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
		window.onclick = function(event) {
			if (!event.target.matches('.dropbtn')) {
				var dropdowns = document.getElementsByClassName("dropdown-content");
				var i;
				for (i = 0; i < dropdowns.length; i++) {
				  var openDropdown = dropdowns[i];
				  if (openDropdown.classList.contains('show')) {
					openDropdown.classList.remove('show');
				  }
				}
			}
		}
	}

	pokazi(arg){
		let i=1;
		for(i=1;i<6;i++){
			document.getElementById("myTable"+i).style.display = 'none';
		}
		document.getElementById("myTable"+arg).style.display = 'block';
	}

	sortTable(arg,n) {
	  let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById(arg);
	  switching = true;
	  // Set the sorting direction to ascending:
	  dir = "asc";
	  /* Make a loop that will continue until
	  no switching has been done: */
	  while (switching) {
		// Start by saying: no switching is done:
		switching = false;
		rows = table.getElementsByTagName("tr");
		/* Loop through all table rows (except the
		first, which contains table headers): */
		for (i = 1; i < (rows.length - 1); i++) {
		  // Start by saying there should be no switching:
		  shouldSwitch = false;
		  /* Get the two elements you want to compare,
		  one from current row and one from the next: */
		  x = rows[i].getElementsByTagName("TD")[n];
		  y = rows[i + 1].getElementsByTagName("TD")[n];
		  /* Check if the two rows should switch place,
		  based on the direction, asc or desc: */
		  if (dir == "asc") {
			if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
			  // If so, mark as a switch and break the loop:
			  shouldSwitch= true;
			  break;
			}
		  } else if (dir == "desc") {
			if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
			  // If so, mark as a switch and break the loop:
			  shouldSwitch= true;
			  break;
			}
		  }
		}
		if (shouldSwitch) {
		  /* If a switch has been marked, make the switch
		  and mark that a switch has been done: */
		  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
		  switching = true;
		  // Each time a switch is done, increase this count by 1:
		  switchcount ++;
		} else {
		  /* If no switching has been done AND the direction is "asc",
		  set the direction to "desc" and run the while loop again. */
		  if (switchcount == 0 && dir == "asc") {
			dir = "desc";
			switching = true;
		  }
		}
	  }
	}
	render() {
		return (
		<div className="analiza-prometa">
			<body>
				<div class="dropdown">
				  <button onClick={() => this.myFunction()} class="dropbtn">Tablice</button>
				  <div id="myDropdown" class="dropdown-content">
					<a class="linkovi" onClick={() => this.pokazi(1)}>Gradovi</a>
					<a class="linkovi" onClick={() => this.pokazi(2)}>Države</a>
					<a class="linkovi" onClick={() => this.pokazi(3)}>Djelatnosti</a>
					<a class="linkovi" onClick={() => this.pokazi(4)}>Aktivnost</a>
					<a class="linkovi" onClick={() => this.pokazi(5)}>Ključne riječi</a>
				  </div>
				</div>
				<table class="tablica" id="myTable1">
				  <tr class="header">
						<th onClick={() => this.sortTable("myTable1",0)} >Grad</th>
						<th onClick={() => this.sortTable("myTable1",1)} >Broj neprijavljenih korisnika</th>
						<th onClick={() => this.sortTable("myTable1",2)} >Broj prijavljenih korisnika</th>
				  </tr>
				  <tr>
					<td>Zagreb</td>
					<td>25</td>
					<td>50</td>
				  </tr>
				  <tr>
					<td>Split</td>
					<td>11</td>
					<td>32</td>
				  </tr>
				  <tr>
					<td>Velika Gorica</td>
					<td>200</td>
					<td>11</td>
				  </tr>
				</table>
				<table class="tablica" id="myTable2">
				  <tr class="header">
						<th onClick={() => this.sortTable("myTable2",0)} styles="width:50%;">Država</th>
						<th onClick={() => this.sortTable("myTable2",1)} styles="width:25%;">Broj neprijavljenih korisnika</th>
						<th onClick={() => this.sortTable("myTable2",2)} styles="width:25%;">Broj prijavljenih korisnika</th>
				  </tr>
				  <tr>
					<td>Zvonimir</td>
					<td>40000</td>
					<td>100000</td>
				  </tr>
				  <tr>
					<td>Bosna i Hercegovina</td>
					<td>10000</td>
					<td>20000</td>
				  </tr>
				  <tr>
					<td>Slovenija</td>
					<td>5</td>
					<td>20</td>
				  </tr>
				</table>
				<table class="tablica" id="myTable3">
				  <tr class="header">
						<th onClick={() => this.sortTable("myTable3",0)} styles="width:60%;">Djelatnost</th>
						<th onClick={() => this.sortTable("myTable3",1)} styles="width:40%;">Broj korisnika</th>
				  </tr>
				  <tr>
					<td>Zvonimir</td>
					<td>0</td>
				  </tr>
				  <tr>
					<td>Marko</td>
					<td>100</td>
				  </tr>
				  <tr>
					<td>Luka</td>
					<td>200</td>
				  </tr>
				</table>
				<table class="tablica" id="myTable4">
				  <tr class="header">
						<th onClick={() => this.sortTable("myTable4",0)} styles="width:50%;">Ime korisnika</th>
						<th onClick={() => this.sortTable("myTable4",1)} styles="width:25%;">Zadnja aktivnost</th>
						<th onClick={() => this.sortTable("myTable4",2)} styles="width:25%;">Broj poruka</th>
				  </tr>
				  <tr>
					<td>Zvonimir</td>
					<td>0</td>
					<td>0</td>
				  </tr>
				  <tr>
					<td>Marko</td>
					<td>100</td>
					<td>100</td>
				  </tr>
				  <tr>
					<td>Luka</td>
					<td>200</td>
					<td>200</td>
				  </tr>
				</table>
				<table class="tablica" id="myTable5">
				  <tr class="header">
						<th onClick={() => this.sortTable("myTable5",0)} styles="width:60%;">Ključne riječi</th>
						<th onClick={() => this.sortTable("myTable5",1)} styles="width:40%;">Broj pretraga</th>
				  </tr>
				  <tr>
					<td>Zvonimir</td>
					<td>0</td>
				  </tr>
				  <tr>
					<td>Marko</td>
					<td>100</td>
				  </tr>
				  <tr>
					<td>Luka</td>
					<td>200</td>
				  </tr>
				</table>
			</body>
		</div>
		);
	}
}
export default Traffic;

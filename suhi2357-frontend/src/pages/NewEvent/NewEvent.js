import React, { Component } from 'react';
import './NewEvent.css';
import {Link} from 'react-router';

class NewEvent extends Component {
  render() {
    return (
      <div className="NewEvent">
        <h1>Stvori novi događaj</h1>
		  <input placeholder="naziv događaja" type="naziv događaja"/>
		  <input placeholder="mjesto događaja" type="mjesto događaja"/>
		  <input placeholder="vrijeme događaja" type="vrijeme događaja"/>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default NewEvent;

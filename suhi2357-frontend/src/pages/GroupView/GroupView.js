import React, { Component } from 'react';
import './GroupView.css';
import {Link} from 'react-router';
import ReactDatalist from 'react-datalist';
import Announcement from '../../components/Announcement/Announcement';
import {runInAction} from 'mobx';
import {fetchLoginToken, postData} from '../../constants/communication';
import {fetchData} from '../../constants/communication';
import {register} from "../../constants/communication";
import paths from '../../constants/paths';
import SearchBar from '../../components/SearchBar/SearchBar';
import Accordion from '../../components/Accordion/Accordion';
import {read} from '../../services/storage';

import logoURL5 from '../../assets/background.jpeg';


class GroupView extends Component {
	constructor(args) {
		super(args);
		this.state = {
			users: [],
		};
	}
	getUsers() {


	    //registriraj novog coveka
        var data = {
            "firstName": "josko",
            "lastName": "lokas",
            "email": "loc@gmail.com",
            "companyId": "1",
            "phoneNumber": "3453436345",
            "activity": "zajebancija"
        };

        // register(data).then((res) => {
        //     runInAction(() => {
        //         console.log('dafuk');
        //     })
        // });


	    //logiraj se i uzmi sve usere npr.
			fetchData(paths.restApi.fetchCompanies, read('token'))
				.then((res) => {
					this.setState({
			      users: res
			    });
					console.log(this.state.users);
					console.log("uspio");
				});
				let d = {
					"name" : "sdseee",
					"address" : "sadsds",
					"activityId" : 11
				}
				// postData(paths.restApi.createCompany, read('token'), d);
        // fetchLoginToken("email2@email.com", "password")
        //     .then((res) => {
        //         runInAction(() => {
        //             if (res.error) {
        //                 console.log('error!!!!!');
        //             }
        //             console.log(res.access_token);
				// 						let d = {
				// 							"name" : "jedna",
				// 							"address" : "prva",
				// 							"activityId" : 1
				// 						}
				// 						// postData(paths.restApi.createCompany, res.access_token, d);
        //             fetchData(paths.restApi.fetchUsers, res.access_token)
        //                 .then ((res2) => {
        //                 runInAction(() => {
        //                     console.log(res2);
        //                 })
        //                 });
        //
        //         });
        //     })

	}


    componentDidMount() {
		this.getUsers();
	}

	search() {
	  let input, filter, table, tr, td, i;
	  input = document.getElementById("myInput5");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("myTable5");
	  tr = table.getElementsByTagName("tr");

	  for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[0];
		if (td) {
		  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
			tr[i].style.display = "";
		  } else {
			tr[i].style.display = "none";
		  }
		}
	  }
	}

  render() {
    return (
			<div className="GroupView">
				<div className="evgrpoc">
					<div className="osnovneinfo">
						<h1>Događaj</h1>
						<p>Vlasnik:</p>
						<p>Nastala:</p>
						<p>Broj članova:</p>
					</div>
					<div class="slika">
						<img class="wallpaper" src={logoURL5}/>
					</div>
					<div class="tabla">
						<input type="text" id="myInput5" onKeyUp={() => this.search()} placeholder="Traži po imenu..." title="Type in a name"/>
						<table class="tablicaClanova" id="myTable5">
							<tr class="headerClanova">
								<th>Članovi grupe:</th>
							</tr>
							<tr>
								<td>Zvonimir</td>
							</tr>
							<tr>
								<td>Marko</td>
							</tr>
							<tr>
								<td>Luka</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		);
  }
}

export default GroupView;

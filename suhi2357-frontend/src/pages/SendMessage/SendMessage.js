import React, { Component } from 'react';
import './SendMessage.css';
import {Link} from 'react-router';

class SendMessage extends Component {
  render() {
    return (
      <div className="SendMessage">
        <h1>Pošalji poruku</h1>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default SendMessage;

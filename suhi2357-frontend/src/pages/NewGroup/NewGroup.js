import React, { Component } from 'react';
import './NewGroup.css';
import {Link} from 'react-router';

class NewGroup extends Component {
  render() {
    return (
      <div className="NewGroup">
        <h1>Stvori novu grupu</h1>
		  <input placeholder="naziv grupe" type="naziv grupe"/>
		  <input placeholder="email člana" type="email"/>
		  <button>Dodaj člana</button>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default NewGroup;

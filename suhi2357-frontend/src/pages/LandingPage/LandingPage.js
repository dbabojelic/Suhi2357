import React, { Component } from 'react';
import './LandingPage.css';
import {Link} from 'react-router';

class LandingPage extends Component {
  render() {
    return (
      <div className="LandingPage">
        <h1>Vidi osnovne informacije</h1>
        <li><Link to="/">Natrag</Link></li>
      </div>
    );
  }
}

export default LandingPage;

import React, { Component } from 'react';
import './Home.css';
import {Link} from 'react-router';
import {runInAction} from 'mobx';
import {fetchLoginToken, postData} from '../../constants/communication';
import {fetchData} from '../../constants/communication';
import {register} from "../../constants/communication";
import paths from '../../constants/paths';
import SearchBar from '../../components/SearchBar/SearchBar';
import Accordion from '../../components/Accordion/Accordion';
import Announcement from '../../components/Announcement/Announcement';
import {read, save} from '../../services/storage';
import ReactDatalist from 'react-datalist';
import searchURL from '../../assets/search.png';

class Home extends Component {
	constructor(args) {
		super(args);
		this.state = {
			users: [],
			me: {},
			myGroups: [],
			notifications: [],
			posaljiNotifId: 3,
			invitedGroups: [],
			memberGroups: [],
			memberEvents: []
		};
	}
	openModal(tId) {
		this.setState({
		      posaljiNotifId: tId
		});
    document.getElementById("posaljiModal").style.display = "block";
  }
	closeModal() {
    document.getElementById("posaljiModal").style.display = "none";
  }
	getUsers() {


	    //registriraj novog coveka
        // var data = {
        //     "firstName": "josko",
        //     "lastName": "lokas",
        //     "email": "loc@gmail.com",
        //     "companyId": "1",
        //     "phoneNumber": "3453436345",
        //     "activity": "zajebancija"
        // };

        // register(data).then((res) => {
        //     runInAction(() => {
        //         console.log('dafuk');
        //     })
        // });


	    //logiraj se i uzmi sve usere npr.
			// fetchData(paths.restApi.fetchCompanies, read('token'))
			// 	.then((res) => {
			// 		this.setState({
			//       users: res
			//     });
			// 		console.log(this.state.users);
			// 		console.log("uspio");
			// 	});
				// let d = {
				// 	"name" : "sdefgtgt",
				// 	"address" : "4543",
				// 	"activityId" : 555
				// };
				// postData(paths.restApi.createCompany, read('token'), d);
        // fetchLoginToken("email2@email.com", "password")
        //     .then((res) => {
        //         runInAction(() => {
        //             if (res.error) {
        //                 console.log('error!!!!!');
        //             }
        //             console.log(res.access_token);
				// 						let d = {
				// 							"name" : "jedna",
				// 							"address" : "prva",
				// 							"activityId" : 1
				// 						}
				// 						// postData(paths.restApi.createCompany, res.access_token, d);
        //             fetchData(paths.restApi.fetchUsers, res.access_token)
        //                 .then ((res2) => {
        //                 runInAction(() => {
        //                     console.log(res2);
        //                 })
        //                 });
				//
				//         });
				//     })

			}


			componentDidMount() {
				fetchData(paths.restApi.fetchUsers, read('token'))
		    .then((res) => {
		      this.setState({
		        users: res
		      });
		    });
				fetchData(paths.restApi.fetchWhoAmI, read('token'))
				.then((res) => {
					save('mojId', res.id);
					this.setState({
						me: res
					});
					console.log("uspio222");
					console.log(this.state.me);
					fetchData(paths.restApi.fetchGroupsByInvitedId(this.state.me.id), read('token'))
					.then((res) => {
						this.setState({
							invitedGroups: res
						});
						console.log("uspioGrupe");
						console.log(this.state.invitedGroups);
					});
					fetchData(paths.restApi.fetchEvents, read('token'))
					.then((res) => {
						this.setState({
							memberEvents: res
						});
						console.log("uspioEvents");
						console.log(this.state.memberEvents);
					});
					fetchData(paths.restApi.fetchGroupsByMemberId(this.state.me.id), read('token'))
					.then((res) => {
						this.setState({
							memberGroups: res
						});
						console.log("uspioMember");
						console.log(this.state.memberGroups);
					});
					fetchData(paths.restApi.fetchNotificationsByRecieverId(this.state.me.id), read('token'))
					.then((res) => {
						this.setState({
							notifications: res
						});
						console.log("uspio333");
						console.log(this.state.notifications);
					});
					fetchData(paths.restApi.fetchGroupsByMemberId(this.state.me.id), read('token'))
						console.log(this.state.users);
				});

				this.getUsers();

			}
			posaljiNotif() {
				let data = {}
				data["typeId"] = this.state.posaljiNotifId;
				let msgVal = document.getElementById("notifMsg").value;
				data["message"] = msgVal;
				data["senderId"] = this.state.me.id;
				if (this.state.posaljiNotifId === 1) {
					let inputUser = document.getElementById("korisnikNotif").value.split(" ");
			    let userId = this.state.users.filter((k) => k.firstName === inputUser[0] && k.lastName === inputUser[1])[0].id;
					data["id"] = userId;
				} else if (this.state.posaljiNotifId === 2) {
					let inputGrad = document.getElementById("grupaNotif").value;
			    let gradId = this.state.myGroups.filter((grad) => grad.name === inputGrad)[0].id;
					data["id"] = gradId;
				} else {
					data["id"] = -1;
				}
				postData(paths.restApi.createNotification, read('token'), data)
				.then((res) => {
					alert("obavijest poslana");
					this.closeModal();
				});

				console.log(data);
			}

			render() {
				return (
			<div className="main">
				<div className="miniSearch">
					<img className="searchIcon" src={searchURL}/>
					<ReactDatalist className="searchBar" list="fruit" options={this.state.users.map((a) => a.name)} />
					<button >Go!</button>
				</div>
				<h2>Posljednje obavijesti:</h2>
				<div className="oglasnaPloca">
					{
						this.state.notifications.map((notif) => <Announcement notif={notif}/>)
					}
				</div>
				<div className="saljiStvari">
					<button onClick={() => this.openModal(1)}>Pošalji obavijest članu</button>
					<button onClick={() => this.openModal(2)}>Pošalji obavijest grupi</button>
					<button onClick={() => this.openModal(3)}>Pošalji obavijest događaju</button>
				</div>
				<div id="posaljiModal" className="modal">
          <div className="modal-content">
						<span onClick={() => this.closeModal()} className="close">&times;</span>
						{
							this.state.posaljiNotifId === 3 ? "Poruka svim korisnicima:" : this.state.posaljiNotifId === 1 ?
							<div className="osobnaPoruka">
								<p>Pošalji poruku članu:</p>
								<input placeholder="Ime korisnika" list="korisnici" id="korisnikNotif" required/>
								<datalist id="korisnici">
									{this.state.users.map((item) =>
										<option value={item.firstName.concat(" ").concat(item.lastName)}/>
									)}
								</datalist>
							</div>
							 :
							 <div className="grupnaPoruka">
								 <p>Pošalji poruku grupi:</p>
 								<input placeholder="Ime grupe" list="grupe" id="grupaNotif" required/>
 								<datalist id="korisnici">
 									{this.state.myGroups.map((item) =>
 										<option value={item.name}/>
 									)}
 								</datalist>
 							</div>
						}
						<p>Poruka:</p>
						<textarea palceholder="Poruka..." id="notifMsg" rows="5" cols="50"></textarea>
						<button onClick={() => this.posaljiNotif()}>Podnesi</button>
          </div>
        </div>
				{/* <SearchBar/> */}
				<Accordion invitedGroups={this.state.invitedGroups} memberGroups={this.state.memberGroups} events={this.state.memberEvents}/>
			</div>
			);
		}
	}

export default Home;

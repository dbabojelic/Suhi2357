import React, { Component } from 'react';
import './EventDetails.css';
import {Link} from 'react-router';

class EventDetails extends Component {
  render() {
    return (
      <div className="EventView">
        <h1>Vidi događaj</h1>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default EventDetails;

import React, { Component } from 'react';
import './FeesView.css';
import {Link} from 'react-router';

class FeesView extends Component {
	mod(){
		document.getElementById("myMod1").style.display = "block";
    }

	closeMod(){
		document.getElementById("myMod1").style.display = "none";
    }

	obradipromjenu(arg){
		document.getElementById("probica").innerHTML = arg;
		this.closeMod();
	}

	search() {
	  let input, filter, table, tr, td, i;
	  input = document.getElementById("myInput");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("myTable");
	  tr = table.getElementsByTagName("tr");

	  for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[0];
		if (td) {
		  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
			tr[i].style.display = "";
		  } else {
			tr[i].style.display = "none";
		  }
		}
	  }
	}
	sortTable(n) {
	  let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById("myTable");
	  switching = true;
	  // Set the sorting direction to ascending:
	  dir = "asc";
	  /* Make a loop that will continue until
	  no switching has been done: */
	  while (switching) {
		// Start by saying: no switching is done:
		switching = false;
		rows = table.getElementsByTagName("tr");
		/* Loop through all table rows (except the
		first, which contains table headers): */
		for (i = 1; i < (rows.length - 1); i++) {
		  // Start by saying there should be no switching:
		  shouldSwitch = false;
		  /* Get the two elements you want to compare,
		  one from current row and one from the next: */
		  x = rows[i].getElementsByTagName("TD")[n];
		  y = rows[i + 1].getElementsByTagName("TD")[n];
		  /* Check if the two rows should switch place,
		  based on the direction, asc or desc: */
		  if (dir == "asc") {
			if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
			  // If so, mark as a switch and break the loop:
			  shouldSwitch= true;
			  break;
			}
		  } else if (dir == "desc") {
			if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
			  // If so, mark as a switch and break the loop:
			  shouldSwitch= true;
			  break;
			}
		  }
		}
		if (shouldSwitch) {
		  /* If a switch has been marked, make the switch
		  and mark that a switch has been done: */
		  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
		  switching = true;
		  // Each time a switch is done, increase this count by 1:
		  switchcount ++;
		} else {
		  /* If no switching has been done AND the direction is "asc",
		  set the direction to "desc" and run the while loop again. */
		  if (switchcount == 0 && dir == "asc") {
			dir = "desc";
			switching = true;
		  }
		}
	  }
	}
	render() {
		return (
		<div className="main-content">
			<body>
			<div id="myMod1" className="modal">
				<div className="modal-content">
				  <span onClick={() => this.closeMod()} className="close">&times;</span>
				  <p>Nova članarina</p>
				  <input id="upisi" placeholder="Upiši novu članarinu..."/>
				  <button onClick={() => this.obradipromjenu(document.getElementById("upisi").value)}>Promijeni članarinu</button>
				</div>
			</div>
				<h2>Vidi članarine</h2>
				<input type="text" id="myInput" onKeyUp={() => this.search()} placeholder="Traži po imenu..." title="Type in a name"/>
				<table id="myTable">
				  <tr class="header">
						<th onClick={() => this.sortTable(0)} styles="width:50%;">Ime</th>
						<th onClick={() => this.sortTable(1)} styles="width:25%;">Preostala članarina(dani)</th>
						<th styles="width:25%;">Promijeni članarinu</th>
				  </tr>
				  <tr>
					<td>Zvonimir</td>
					<td id="probica">0</td>
					<button onClick={() => this.mod("1")}>Promijeni članarinu</button>
				  </tr>
				  <tr>
					<td>Marko</td>
					<td>100</td>
					<button onClick={() => this.mod("1")}>Promijeni članarinu</button>
				  </tr>
				  <tr>
					<td>Luka</td>
					<td>200</td>
					<button onClick={() => this.mod("1")}>Promijeni članarinu</button>
				  </tr>
				</table>
			</body>
		</div>
		);
	}
}

export default FeesView;

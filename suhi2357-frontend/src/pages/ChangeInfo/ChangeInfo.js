import React, { Component } from 'react';
import './ChangeInfo.css';
import {Link} from 'react-router';
import logoURL2 from '../../assets/logo.PNG';
import logoURL3 from '../../assets/avatar.jpg';
import logoURL4 from '../../assets/poduzetnici.png';
import {read} from '../../services/storage';
import {fetchUnauthCompaines, postData, fetchSlika, fetchData} from '../../constants/communication';
import paths from '../../constants/paths';


let BACKEND = "http://138.68.92.193:8080"
// let BACKEND = "http://localhost:8080"
let myId = read('mojId');

class ChangeInfo extends Component {
  constructor(args) {
		super(args);
		this.state = {
			pathToAvatar: "",
            currentUser: {},
            company: {},
            cityName: ""
		};
	}
  chinfo(){
    let x1 = document.getElementById("ime").value;
    let x2 = document.getElementById("prez").value;
    let x3 = document.getElementById("imep").value;
    let x4 = document.getElementById("adp").value;
    let x5 = document.getElementById("email").value;
    let x6 = document.getElementById("tel").value;

    document.getElementById("pimeprez").innerHTML = x1+" "+x2;
    document.getElementById("pimep").innerHTML = x3;
    document.getElementById("padp").innerHTML = x4;
    document.getElementById("pemail").innerHTML = x5;
    document.getElementById("ptel").innerHTML = x6;
  }
  componentDidMount() {
    fetchSlika(paths.restApi.fetchPathToUserAvatar, read('token'))
    .then((res) => {
      this.setState({
        pathToAvatar: res.path
      });
      console.log("uspio333");
      console.log(this.state.pathToAvatar)
    });
    fetchData(paths.restApi.fetchWhoAmI, read('token'))
        .then((res) => {
      this.setState({currentUser: res})
            console.log(res);
            fetchData(paths.restApi.fetchCompaniesByEmployeeId(res.id), read('token')).then((res2) => {
              this.setState({company: res2})
                console.log(res2);
            })

        });
  }
  render() {
    return (
      <div className="ChangeInfo">
        <div class="card">
          <iframe width="0" height="0" border="0" name="dummyframe" id="dummyframe"></iframe>
          <img class="profile-pic" src={BACKEND + this.state.pathToAvatar} id="profilnaslika"/>
          <form enctype="multipart/form-data" action={BACKEND + "/unauthorized/image/avatar/" + myId} method="post" target="dummyframe">
            <input id="file" type="file" name="file"/>
            <input id="submit" type="submit" />
          </form>
        <h1 id="pimeprez">{this.state.currentUser.firstName} {this.state.currentUser.lastName}</h1>
        <p id="pimep" className="ci__inputi">Naziv poduzeća: {this.state.company.name}</p>
        <p id="padp" className="ci__inputi">Adresa: {this.state.currentUser.address}</p>
        <p id="padp" className="ci__inputi">Grad: Zagreb</p>
        <p id="pemail" className="ci__inputi">Email: {this.state.currentUser.email}</p>
        <p id="ptel" className="ci__inputi">Kontakt: {this.state.currentUser.phoneNumber}</p>
          <p id="ptel" className="ci__inputi">Djelatnost: {this.state.currentUser.activityDescription}</p>

      </div>

    </div>




  );
}
}

export default ChangeInfo;

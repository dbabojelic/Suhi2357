import './Registration.css';

import {Link, Redirect} from 'react-router';
import {save, read} from '../../services/storage';
import ReactDatalist from 'react-datalist';

import {fetchUnauthCompaines, postData, register} from '../../constants/communication';
import {fetchData} from '../../constants/communication';
import paths from '../../constants/paths';

import {runInAction} from 'mobx';

import React, { Component } from 'react';
import {render} from 'react-dom';
import {useStrict} from 'mobx';
import {Router, Route, browserHistory, IndexRoute, hashHistory} from 'react-router';

useStrict(true);

class Registration extends Component {
  constructor(args) {
    super(args);
    this.state = {
      name: '',
      surname: '',
      email: '',
      company: '',
      address: '',
      telephone: '',
      password: '',
      passwordRepeat: '',
      companies: [],
      cities: [],
      tvrtke: ['Adris grupa', 'Atlantic grupa', 'Dalekovod', 'Ingra', 'Končar', 'Podravka']
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  getCompanies() {
    fetchUnauthCompaines(paths.unauthorized.companies)
    .then((res) => {
      this.setState({
        companies: res
      });
    });
    fetchUnauthCompaines(paths.unauthorized.city)
    .then((res) => {
      save('cities', res);
      this.setState({
        cities: res
      });
      console.log(read('cities'));
    });
  }
  _onNameInput({target: {value}}) {
    this.setState({
      email: value
    });
  }
  _onSurnameInput({target: {value}}) {
    this.setState({
      email: value
    });
  }
  _onEmailInput({target: {value}}) {
    this.setState({
      email: value
    });
  }
  _onCompanyInput({target: {value}}) {
    this.setState({
      email: value
    });
  }
  _onAddressInput({target: {value}}) {
    this.setState({
      email: value
    });
  }
  _onTelephoneInput({target: {value}}) {
    this.setState({
      email: value
    });
  }
  _onPasswordInput({target: {value}}) {
    this.setState({
      password: value
    });
  }
  _onPasswordRepeatInput({target: {value}}) {
    this.setState({
      passwordRepeat: value
    });
  }
  _onClick() {
    let data = {
        "firstName": "josko",
        "lastName": "lokas",
        "email": "loc@gmail.com",
        "companyId": "1",
        "phoneNumber": "3453436345",
        "activity": "zajebancija"
    };
  }
  componentDidMount() {
    this.getCompanies();
  }
  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    if (data.get('password') !== data.get('password2')) {
      alert('Lozinke nisu jednake. Molimo pokušajte ponovno.');
      return;
    }
    let inputGrad = document.getElementById("input2").value;
    let gradId = this.state.cities.filter((grad) => grad.name === inputGrad)[0].id;
    data.append('cityId', gradId);
    data.append('activity', "sssss");
    data.append('companyId', 1);
    data.delete('password2');
    let formObj = {};
    data.forEach(function(value, key){
      formObj[key] = value;
    });
    register(formObj).then((res) => {
        runInAction(() => {
            alert("Uspješna registracija! Molimo prijavite se s novim podacima.");
            browserHistory.push("/");
        })
    });
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
      <div className="Registration">
        <div className="reg-container">
          <div className="reg-header">
            <h3 className="naslov">Registracija</h3>
            <p>Upišite podatke (sva su polja obavezna):</p>
          </div>
          <div className="reg-form">
            <div className="inputContainer">
              <div className="inputi1">
                <span>Ime:</span>
                <input name="firstName" onChange={(e) => this._onNameInput(e)} placeholder="ime" type="Ime" required/>
                <span>Prezime:</span>
                <input name="lastName" onChange={(e) => this._onNameInput(e)} placeholder="prezime" type="Prezime" required/>
                <span>E-mail adresa:</span>
                <input name="email" onChange={(e) => this._onNameInput(e)} placeholder="email" type="email" required/>
              </div>
              <div className="inputi2">
                <span>Naziv poduzeca:</span>
                {/* <input onChange={(e) => this._onNameInput(e)} placeholder="naziv poduzeća" type="naziv poduzeca"/> */}
                <input placeholder="odaberi tvrtku" list="companies" id="input1" required/>
                <datalist id="companies">
                  {this.state.companies.map((item) =>
                    <option value={item.name}/>
                  )}
                </datalist>
                <span>Grad:</span>
                <input placeholder="odaberi grad" list="cities" id="input2" required/>
                <datalist id="cities">
                  {this.state.cities.map((item) =>
                    <option value={item.name}/>
                  )}
                </datalist>
                <span>Ulica i kućni broj:</span>
                <input name="address" onChange={(e) => this._onNameInput(e)} placeholder="adresa stanovanja" type="adresa poduzeca" required/>
              </div>
              <div className="inputi3">
                <span>Broj telefona:</span>
                <input name="phoneNumber" onChange={(e) => this._onNameInput(e)} placeholder="broj telefona" type="broj telefona" required/>
                <span>Lozinka:</span>
                <input name="password" onChange={(e) => this._onPasswordInput(e)} placeholder="lozinka" type="Password" required/>
                <span>Potvrdi lozinku:</span>
                <input name="password2" onChange={(e) => this._onPasswordRepeatInput(e)} placeholder="potvrdi lozinku" type="Password" required/>
              </div>
            </div>
            <button onClick={() => this._onClick()} >Registracija</button>
          </div>
          <li className="natrag"><Link to="/">Natrag</Link></li>
        </div>
      </div>
    </form>

    );
  }
}
export default Registration;

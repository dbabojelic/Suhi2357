import React, { Component } from 'react';
import './Reports.css';
import {Link} from 'react-router';

import SideNav from '../../components/SideNav/SideNav';

class Reports extends Component {
  render() {
    return (
      <div className="Reports">
        <SideNav>
          <h1>Pregled izvještaja</h1>
        </SideNav>
      </div>
    );
  }
}

export default Reports;

import React, { Component } from 'react';
import './Error.css';
import {Link} from 'react-router';

class Error extends Component {
  render() {
    return (
      <div className="Error">
        <h1>Greška!</h1>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default Error;

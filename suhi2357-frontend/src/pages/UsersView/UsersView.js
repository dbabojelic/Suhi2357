import React, { Component } from 'react';
import './UsersView.css';
import {Link} from 'react-router';

class UsersView extends Component {
  render() {
    return (
      <div className="UsersView">
        <h1>Pregled korisnika</h1>
        <li><Link to="/home">Natrag</Link></li>
      </div>
    );
  }
}

export default UsersView;

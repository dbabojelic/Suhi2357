import React, { Component } from 'react';
import './App.css';
import {runInAction} from 'mobx';
import logoURL from './assets/logo.PNG';
import {save} from './services/storage';
import {browserHistory} from 'react-router';

import poduzetnikURL from './assets/poduzetnici.png';
import videoURL from './assets/video.mp4';
import {Link} from 'react-router';

import {fetchLoginToken, postData} from './constants/communication';
import {fetchData} from './constants/communication';
import {register} from "./constants/communication";
import paths from './constants/paths';

class App extends Component {
  constructor(args) {
    super(args);
    this.state = {
      email: '',
      password: '',
      wrong: false
    };
  }
  _onEmailInput({target: {value}}) {
    this.setState({
      email: value
    });
  }
  _onPasswordInput({target: {value}}) {
    this.setState({
      password: value
    });
  }
  _onClick() {
    fetchLoginToken(this.state.email, this.state.password)
    .then((res) => {
      runInAction(() => {
        if (res.error) {
          // alert("Neispravni login podaci. Molimo unesite ispravne podatke.");
          this.setState({
            wrong: true
          });
        } else {
          save('token', res.access_token);
          browserHistory.push('/home');
        }
      });
    })

  }
  render() {
    return (
      <div className="App">
        <video className="myVideo" loop autoPlay>
          <source src={videoURL} type="video/mp4" />
          <source src={videoURL} type="video/ogg" />
          Your browser does not support the video tag.
        </video>
        <div className="app-content">
          <h1>Dobrodošli na stranice Sustava udruge poduzetnika</h1>
          <div className="Container">
            <div className="info">
              <span className="spen">Sustav udruge poduzetnika FER™</span>
              <span className="spen">Vlasnik: Pero Perić</span>
              <span className="spen">Osnovana: 08.01.2018.</span>
            </div>
            <div>
              <img className="app-logo" src={logoURL} />
            </div>
          </div>
          <div className="app-inputs">
            <input onChange={(e) => this._onEmailInput(e)} placeholder="email" type="Email"/>
            <input onChange={(e) => this._onPasswordInput(e)} placeholder="lozinka" type="Password"/>
            <button onClick={() => this._onClick()} >Prijava</button>
            {this.state.wrong ? <div class="wrong">Neispravni email ili lozinka</div> : <div class="wrong" />}
          </div>
          <li><Link to="/registration">Registriraj se</Link></li>
        </div>
      </div>

    );
  }
}

export default App;
